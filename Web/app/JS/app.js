angular.module('KombiWorldApp', ['KombiWorldApp.companies',
                                 'KombiWorldApp.site',
                                 'KombiWorldApp.userProfile',
								 'ui.router',
                                 'LocalStorageModule',
                                 'angular-jwt',
                                 'pathgather.popeye'])
.constant('CONFIG', {
    APIURL: "https://157.230.157.29/KombiWorld/api/public/index.php/v1/",
})
.config(function($stateProvider, $urlRouterProvider, $httpProvider, jwtInterceptorProvider){

    jwtInterceptorProvider.tokenGetter = function() {

        return JSON.parse(localStorage.getItem('KW.KWSession'));

    };

    $httpProvider.interceptors.push('jwtInterceptor');

    $stateProvider
        .state('site', {
            controller: 'SiteController',
            controllerAs: 'SiteCtl',
            templateUrl : 'JS/Site/home.html'
        })
        .state('companies', {
            url : '/companies',
            controller: 'CompaniesController',
            controllerAs: 'CompCtl',
            templateUrl : 'JS/Companies/home.html'
        });

    $urlRouterProvider.otherwise('/');

})
.config(function Config($httpProvider, jwtOptionsProvider) {

    jwtOptionsProvider.config({
        whiteListedDomains: ['localhost']
    })
})
.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
        .setPrefix('KW')
        .setStorageType('localStorage')
        .setNotify(true, true)
});
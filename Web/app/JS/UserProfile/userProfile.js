var userProfile = angular.module('KombiWorldApp.userProfile', ['ui.router']);

userProfile.config(function($stateProvider, $urlRouterProvider){

    $stateProvider
		.state('userProfile.myReservations', {
            url : '/myReservations',
            templateUrl : 'JS/UserProfile/Views/myReservations.html'
		})
		.state('userProfile.profileData', {
            url : '/profileData',
            templateUrl : 'JS/UserProfile/Views/profileData.html'
		});

    $urlRouterProvider.otherwise('/userProfile');

});

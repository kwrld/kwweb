var site = angular.module('KombiWorldApp.site', ['ui.router']);

site.config(function($stateProvider, $urlRouterProvider){

    $stateProvider
        .state('site.home', {
            url : '/',
            templateUrl : 'JS/Site/Views/portada.html'
        })
        .state('site.login', {
            url: '/login',
            templateUrl: 'JS/Site/Views/login.html'
        })
        .state('site.signin', {
            url: '/signin',
            templateUrl: 'JS/Site/Views/signin.html'
        });

    $urlRouterProvider.otherwise('/');

});

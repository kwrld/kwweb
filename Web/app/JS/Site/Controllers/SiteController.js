var site = angular.module('KombiWorldApp.site');

site.controller("SiteController", ['jwtHelper', 'CONFIG', 'UsersService', '$state', '$stateParams', 'localStorageService',
									function (jwtHelper, CONFIG, UsersService, $state, $stateParams, localStorageService) {

		var self = this;

		/* Expresiones regulares para validación de campos. */

		this.regExpCUIT = '\\d{2}\\-\\d{8}\\-\\d{1}';
		this.regExpTel = '\\d{8,13}';
		this.regExpDNI = '\\d{7,8}';
		this.regExpNombres = '[a-zA-Z ]*$';


		/* Variables para almacenar temporalmente al seleccionar en la vista. */

		this.userLogin = {
			'user':
			{
				'username': '',
				'password': ''
			}
		};

		this.loggedOnUser =
		{
			'username': '',
			'companies': []
		};


		this.logout = function () {

			localStorageService.remove('KWSession');
			self.loggedOnUser = {};
			$state.go('site.home');

		};

		this.login = function () {

			var tokenInfo = localStorageService.get('KWSession');

			if (tokenInfo != null) {

				if (jwtHelper.isTokenExpired(tokenInfo)) {

					self.loggedOnUser.username = '';
					self.loggedOnUser.companies = [];

					return;

				}

				tokenInfo = jwtHelper.decodeToken(tokenInfo);
				self.loggedOnUser.username = tokenInfo['username'];

				return;

			}

			self.loggedOnUser.username = '';

			return;

		};

		this.loginFromSite = function () {

			UsersService.userLogin(self.userLogin).then(function (response){

				var token = response.headers('Authorization');

				/* Horrendamente hardcodeado el "after Bearer" del token, pero así está la cosa... */
				token = token.substr(6, token.length);

				if (jwtHelper.isTokenExpired(token)) {

					self.loggedOnUser.username = '';

					return;

				}

				var tokenInfo = jwtHelper.decodeToken(token);
				self.loggedOnUser.username = tokenInfo['username'];

				localStorageService.set('KWSession', token);
				localStorageService.set('KWCompany', 1);

				$state.go('companies.home');

				return;

			});
		};

		self.login();

	}]);
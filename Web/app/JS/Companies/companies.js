var companies = angular.module('KombiWorldApp.companies', ['ui.router']);

companies.config(function($stateProvider, $urlRouterProvider){

    $stateProvider
        .state('companies.home', {
            url : '/home',
            controller: 'DashboardController',
            controllerAs: 'DshCtl',
            templateUrl : 'JS/Companies/Views/dashboard.html'
        })
        .state('companies.newticket', {
            url : '/newTicket',
            controller: 'ReservationsController',
            controllerAs: 'RsvCtl',
            templateUrl : 'JS/Companies/Views/newTicket.html'
        })
        .state('companies.routes', {
            url : '/routes',
            controller: 'RoutesController',
            controllerAs: 'RtsCtl',
            templateUrl : 'JS/Companies/Views/routes.html'
		})
        .state('companies.stops', {
            url : '/stops',
            controller: 'StopsController',
            controllerAs: 'StpCtl',
            templateUrl : 'JS/Companies/Views/stops.html'
        })
		.state('companies.schedule', {
            url : '/schedule',
            controller: 'ScheduleController',
            controllerAs: 'SchdCtl',
            templateUrl : 'JS/Companies/Views/schedule.html'
		})
        .state('companies.availability', {
            url : '/availability',
            controller: 'AvailabilityController',
            controllerAs: 'AvCtl',
            templateUrl : 'JS/Companies/Views/availability.html'
        })
        .state('companies.tickets', {
            url : '/tickets',
            controller: 'TicketsController',
            controllerAs: 'TcksCtl',
            templateUrl : 'JS/Companies/Views/tickets.html'
        })
		.state('companies.config', {
            url : '/config',
            controller: 'ConfigController',
            controllerAs: 'CfgCtl',
            templateUrl : 'JS/Companies/Views/config.html'
		})
        .state('companies.profile', {
            url : '/profile',
            controller: 'ProfileController',
            controllerAs: 'PrfCtl',
            templateUrl : 'JS/Companies/Views/profile.html'
        });

    $urlRouterProvider.otherwise('/companies/home');

});
var companies = angular.module('KombiWorldApp.companies');

companies.controller('AvailabilityController', ['jwtHelper', 'CONFIG', '$rootScope', 'UsersService', 'RoutesService', 'TripsService', 'AvailabilityService', '$state', '$stateParams', 'localStorageService',
  function (jwtHelper, CONFIG, $rootScope, UsersService, RoutesService, TripsService, AvailabilityService, $state, $stateParams, localStorageService) {

    var self = this;

    this.company = {
      'companyId': ''
    };

    this.routes = [];
    this.trips = [];
    this.tripAvailability = [];

    this.selectedRoute = {};
    this.showTripsDate = new Date();

    this.loggedOnUser =
    {
      'username': ''
    };

    this.getRoutesFromCompany = function () {

      self.routes = [];

      RoutesService.getRoutesFromCompany(self.company.companyId).then(function(response){

        self.routes = response.data.routes;

      });
    };

    this.getRouteTripsByDate = function () {

      self.tripAvailability = [];

      TripsService.getRouteTripsByDate(self.selectedRoute.routeId, moment(self.showTripsDate).format('YYYY-MM-DD')).then(function(response) {

        self.trips = [];
        self.trips = response.data.trips;

      });
    };

    this.getAvailabilityFromTripAndDate = function (aTrip) {

      AvailabilityService.getAvailabilityFromTripAndDate(aTrip.routeId, aTrip.tripDateTime).then(function (response) {

        self.tripAvailability = [];
        self.tripAvailability = response.data.availability;

      });
    };

    this.login = function () {

      var tokenInfo = localStorageService.get('KWSession');
      self.company.companyId = localStorageService.get('KWCompany');

      if (tokenInfo != null) {

        if (jwtHelper.isTokenExpired(tokenInfo)) {

          self.loggedOnUser.username = '';
          self.loggedOnUser.companies = [];

          return;

        }

        tokenInfo = jwtHelper.decodeToken(tokenInfo);
        self.loggedOnUser.username = tokenInfo['username'];
        self.loggedOnUser.companies = tokenInfo['companies'];

        return;

      }

      self.loggedOnUser.username = '';
      self.loggedOnUser.companies = [];

      return;

    };

    this.logout = function () {

      store.remove('token');
      self.loggedOnUser = {};
      $state.go('site.home');

    };

    self.login();
    self.getRoutesFromCompany();

  }]);
var companies = angular.module('KombiWorldApp.companies');

companies.controller('TripsController', ['jwtHelper', 'CONFIG', '$rootScope', 'UsersService',
                                        'RoutesService', 'TripsService', '$state', '$stateParams',
                                        'localStorageService',
                                        function (jwtHelper, CONFIG, $rootScope,
                                                UsersService, RoutesService, TripsService,
                                                $state, $stateParams, localStorageService) {
    var self = this;

    this.company = {
        'companyId': ''
    };

    this.routes = [];
    this.trips = [];

    this.selectedRoute = {};
    this.showTripsDate = new Date();

    this.days = {
        'schedule': {
            'onMondays': 0,
            'onTuesdays': 0,
            'onWednesdays': 0,
            'onThursdays': 0,
            'onFridays': 0,
            'onSaturdays': 0,
            'onSundays': 0,
            'frequency': 0,
            'fromDateTime': '',
            'toDateTime': '',
            'routeId': -1
        }
    };

    this.time = {
        'dateFrom': '',
        'timeFrom': '',
        'dateTo': '',
        'timeTo': ''
    };

    this.loggedOnUser =
    {
        'username': ''
    };

    this.logout = function () {

        store.remove('token');
        self.loggedOnUser = {};
        $state.go('site.home');

    };

    this.generateTrips = function () {

        self.days.schedule.fromDateTime = (moment(self.time.dateFrom).format('YYYY-MM-DD')) + ' ' + moment(self.time.timeFrom).format('HH:mm');
        self.days.schedule.toDateTime = (moment(self.time.dateTo).format('YYYY-MM-DD')) + ' ' + moment(self.time.timeTo).format('HH:mm');

        TripsService.massiveTripsCreation(self.days).then(function(response) {
            console.log(self.days);
        });
    };

    this.getRoutesFromCompany = function () {

        self.routes = [];

        RoutesService.getRoutesFromCompany(self.company.companyId).then(function(response){

            self.routes = response.data.routes;

        });
    };

    this.getStopsAvailabilityByDate = function () {

        TripsService.getStopsAvailabilityByDate(self.days.schedule.routeId, moment(self.showTripsDate).format('YYYY-MM-DD')).then(function(response) {

            self.trips = [];
            self.trips = response.data.trips;

        });
    };

    this.clearTripInfo = function () {

        self.stopInput = {
            'stop': {
                'stopId': '',
                'routeId': '',
                'stopName': '',
                'stopAddress': '',
                'latitude': '',
                'longitude': '',
                'stopElapsedTime': '',
                'stopAdditionalFee': ''
            }
        };

        self.selectedStop = {};
    };

    this.getRouteTripsByDate = function () {

        TripsService.getRouteTripsByDate(self.days.schedule.routeId, moment(self.showTripsDate).format('YYYY-MM-DD')).then(function(response) {

            self.trips = [];
            self.trips = response.data.trips;

        });

    };

    this.dateAfter = function () {

        self.showTripsDate = moment(self.showTripsDate).add(1, 'days').toDate();
        self.getRouteTripsByDate();

    };

    this.dateBefore = function () {

        self.showTripsDate = moment(self.showTripsDate).subtract(1, 'days').toDate();
        self.getRouteTripsByDate();
    };

    this.availabilityDateAfter = function () {

        self.showTripsDate = moment(self.showTripsDate).add(1, 'days').toDate();
        self.getStopsAvailabilityByDate();

    };

    this.availabilityDateBefore = function () {

        self.showTripsDate = moment(self.showTripsDate).subtract(1, 'days').toDate();
        self.getStopsAvailabilityByDate();
    };

    this.login = function () {

        var tokenInfo = localStorageService.get('KWSession');
        self.company.companyId = localStorageService.get('KWCompany');

        if (tokenInfo != null) {

            if (jwtHelper.isTokenExpired(tokenInfo)) {

                self.loggedOnUser.username = '';
                self.loggedOnUser.companies = [];

                return;

            }

            tokenInfo = jwtHelper.decodeToken(tokenInfo);
            self.loggedOnUser.username = tokenInfo['username'];
            self.loggedOnUser.companies = tokenInfo['companies'];

            return;

        }

        self.loggedOnUser.username = '';
        self.loggedOnUser.companies = [];

        return;

    };

    self.login();
    self.getRoutesFromCompany();

}]);
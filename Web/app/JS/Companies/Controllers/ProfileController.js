var companies = angular.module('KombiWorldApp.companies');

companies.controller('ProfileController', ['jwtHelper', 'CONFIG', '$rootScope', 'UsersService',
                                                'RoutesService', 'TripsService', '$state', '$stateParams',
                                                'localStorageService',
                                                function (jwtHelper, CONFIG, $rootScope,
                                                        UsersService, RoutesService, TripsService,
                                                        $state, $stateParams, localStorageService) {
    var self = this;

    this.company = {
        'companyId': ''
    };

    this.routes = [];
    this.trips = [];

    this.selectedRoute = {};
    this.showTripsDate = new Date();

    this.loggedOnUser =
    {
        'username': ''
    };

    this.logout = function () {

        store.remove('token');
        self.loggedOnUser = {};
        $state.go('site.home');

    };

    this.getRoutesFromCompany = function () {

        self.routes = [];

        RoutesService.getRoutesFromCompany(self.company.companyId).then(function(response){

            self.routes = response.data.routes;

        });
    };

    this.getStopsAvailabilityByDate = function (aTrip) {

        TripsService.getStopsAvailabilityByDate(aTrip.routeId, moment(aTrip.tripDateTime).format('YYYY-MM-DD')).then(function(response) {

            self.availability = [];
            self.availability = response.data.availability;

        });
    };

    this.login = function () {

        var tokenInfo = localStorageService.get('KWSession');
        self.company.companyId = localStorageService.get('KWCompany');

        if (tokenInfo != null) {

            if (jwtHelper.isTokenExpired(tokenInfo)) {

                self.loggedOnUser.username = '';
                self.loggedOnUser.companies = [];

                return;

            }

            tokenInfo = jwtHelper.decodeToken(tokenInfo);
            self.loggedOnUser.username = tokenInfo['username'];
            self.loggedOnUser.companies = tokenInfo['companies'];

            return;

        }

        self.loggedOnUser.username = '';
        self.loggedOnUser.companies = [];

        return;

    };

    self.login();
    self.getRoutesFromCompany();

}]);
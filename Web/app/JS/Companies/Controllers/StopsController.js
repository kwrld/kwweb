var companies = angular.module('KombiWorldApp.companies');

companies.controller('StopsController', ['jwtHelper', 'CONFIG', '$rootScope', 'CompaniesService', 'UsersService', 'RoutesService', 'TripsService', 'StopsService', '$state', '$stateParams', 'localStorageService',
  function (jwtHelper, CONFIG, $rootScope, CompaniesService, UsersService, RoutesService, TripsService, StopsService, $state, $stateParams, localStorageService) {

    var self = this;

    this.company = {
      'companyId': ''
    };

    this.routes = [];
    this.stops = [];

    this.selectedRoute = {};
    this.selectedStop = {};

    this.routeInput = {
      'route': {
        'routeId': '',
        'companyId': '',
        'name': '',
        'baseRate': ''
      }
    };

    this.stopInput = {
      'stop': {
        'stopId': '',
        'routeId': '',
        'stopName': '',
        'stopAddress': '',
        'latitude': '',
        'longitude': '',
        'stopElapsedTime': '',
        'stopAdditionalFee': ''
      }
    };

    this.loggedOnUser =
    {
      'username': ''
    };

    this.marker = {
      'id': '',
      'options':{
        'draggable': false
      },
      'coords':{
        'latitude': '',
        'longitude': ''
      }
    };

    this.events = {
      place_changed:function (searchbox) {
        var place = searchbox.getPlace();
        if (!place || place == 'undefined') {
          console.log('no place data :(');
          return;
        }

        // refresh the map
        self.map = {
          center:{
            latitude:place.geometry.location.lat(),
            longitude:place.geometry.location.lng()
          },
          zoom:15
        };

        // refresh the marker
        self.marker = {
          id: 0,
          options:{ draggable:false },
          coords:{
            latitude:place.geometry.location.lat(),
            longitude:place.geometry.location.lng()
          }
        };
      },
      dragend: function (marker, eventName, args) {
        $log.log('marker dragend');
        var lat = marker.getPosition().lat();
        var lon = marker.getPosition().lng();
        $log.log(lat);
        $log.log(lon);
      }
    };

    this.map =  {
      center: {
        'latitude': -34.6036856,
        'longitude': -58.3809922
      },
      searchbox: {
        template:'searchbox.tpl.html',
        events: self.events,
        options:{
          autocomplete:true,
          types:['address']
        }
      },
      zoom: 15
    };

    this.saveStop = function () {

      if (self.stopInput.stop.stopName === '' || self.stopInput.stop.stopAddress === '' ||
        self.stopInput.stop.stopElapsedTime === '' || self.stopInput.stop.stopAdditionalFee === '')
      {
        alert("Debe ingresar información antes de guardar la parada.");
        return;
      }

      if (self.stopInput.stop.stopId)
      {
        self.updateStop();
        return;
      }

      self.stopInput.stop.companyId = self.selectedRoute.route.companyId;
      self.stopInput.stop.routeId = self.selectedRoute.route.routeId;

      StopsService.createStop(self.stopInput).then(function(response){

        self.stops = [];
        self.selectedStop = {};
        self.stopInput.stop = {};
        self.getRouteStops(self.selectedRoute.route.routeId);

      });
    };

    this.saveRoute = function () {

      if (self.routeInput.route.baseRate === '' || self.routeInput.route.name === '')
      {
        alert("Debe ingresar información antes de guardar el recorrido.");
        return;
      }

      if (self.routeInput.route.routeId)
      {
        self.updateRoute();
        return;
      }

      self.routeInput.route.companyId = self.company.companyId;

      RoutesService.createRoute(self.routeInput).then(function(response){

        self.getRoutesFromCompany();

      });
    };

    this.deleteStop = function () {

      StopsService.deleteStop(self.selectedStop.stop.routeId, self.selectedStop.stop.stopId).then(function(response){

        self.stops = [];
        self.selectedStop = {};
        self.stopInput.stop = {};
        self.getRouteStops(self.selectedRoute.route.id_recorrido);

      });
    };

    this.deleteRoute = function () {

      RoutesService.deleteRoute(self.selectedRoute.route.routeId).then(function(response){

        self.routes = [];
        self.stops = [];
        self.selectedStop = {};
        self.selectedRoute = {};
        self.stopInput.stop = {};
        self.routeInput.route = {};
        self.getRoutesFromCompany();

      });
    };

    this.updateStop = function () {

      var auxStop = angular.copy(self.stopInput);

      if (auxStop != {}) {

        StopsService.updateStop(auxStop).then(function(response){

          self.routes = [];
          self.stops = [];
          self.selectedStop = {};
          self.selectedRoute = {};
          self.stopInput.stop = {};
          self.routeInput.route = {};
          self.getRoutesFromCompany();

        });
      };
    };

    this.updateRoute = function () {

      var auxRoute = angular.copy(self.routeInput);

      if (auxRoute != {}) {

        RoutesService.updateRoute(auxRoute).then(function(response){

          self.routes = [];
          self.stops = [];
          self.selectedStop = {};
          self.selectedRoute = {};
          self.stopInput.stop = {};
          self.routeInput.route = {};
          self.getRoutesFromCompany();

        });
      };
    };

    this.getRoutesFromCompany = function () {

      self.routes = [];
      self.stops = [];

      RoutesService.getRoutesFromCompany(self.company.companyId).then(function(response){

        self.routes = response.data.routes;

      });
    };

    this.selectRoute = function (aRoute) {

      self.selectedRoute.route = angular.copy(aRoute);
      self.selectedStop.stop = {};
      self.stopInput.stop = {};
      self.routeInput.route = angular.copy(aRoute);

      self.getRouteStops(aRoute.routeId);
    };

    this.selectStop = function (aStop) {

      self.selectedStop.stop = angular.copy(aStop);
      self.stopInput.stop = angular.copy(aStop);

    };

    this.getRouteStops = function (routeId) {

      StopsService.getAllStops(routeId).then(function(response){

        self.stops = [];
        self.stops = response.data.stops;

      });
    };

    this.clearSelectedRoute = function () {

      self.routeInput = {
        'route': {
          'routeId': '',
          'companyId': '',
          'name': '',
          'baseRate': ''
        }
      };

      self.stopInput = {
        'stop': {
          'stopId': '',
          'routeId': '',
          'stopName': '',
          'stopAddress': '',
          'latitude': '',
          'longitude': '',
          'stopElapsedTime': '',
          'stopAdditionalFee': ''
        }
      };

      self.selectedRoute = {};
      self.selectedStop = {};
    };

    this.clearSelectedStop = function () {

      self.stopInput = {
        'stop': {
          'stopId': '',
          'routeId': '',
          'stopName': '',
          'stopAddress': '',
          'latitude': '',
          'longitude': '',
          'stopElapsedTime': '',
          'stopAdditionalFee': ''
        }
      };

      self.selectedStop = {};
    };

    this.login = function () {

      var tokenInfo = localStorageService.get('KWSession');
      self.company.companyId = localStorageService.get('KWCompany');

      if (tokenInfo != null) {

        if (jwtHelper.isTokenExpired(tokenInfo)) {

          self.loggedOnUser.username = '';
          self.loggedOnUser.companies = [];

          return;

        }

        tokenInfo = jwtHelper.decodeToken(tokenInfo);
        self.loggedOnUser.username = tokenInfo['username'];
        self.loggedOnUser.companies = tokenInfo['companies'];

        return;

      }

      self.loggedOnUser.username = '';
      self.loggedOnUser.companies = [];

      return;

    };

    this.logout = function () {

      store.remove('token');
      self.loggedOnUser = {};
      $state.go('site.home');

    };

    self.login();
    self.getRoutesFromCompany();

  }]);
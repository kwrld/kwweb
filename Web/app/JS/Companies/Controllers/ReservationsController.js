var companies = angular.module('KombiWorldApp.companies');

companies.controller('ReservationsController', ['jwtHelper', 'CONFIG', '$rootScope', 'UsersService', 'RoutesService', 'StopsService', 'TripsService', '$state', '$stateParams', 'localStorageService', 'Popeye',
                                                function (jwtHelper, CONFIG, $rootScope, UsersService, RoutesService, StopsService, TripsService, $state, $stateParams, localStorageService, Popeye) {
    var self = this;

    this.company = {
        'companyId': ''
    };

    this.routes = [];
    this.trips = [];
    this.initialStops = [];
    this.finalStops = [];

    this.selectedRoute = {};
    this.selectedInitialStop = {};
    this.selectedFinalStop = {};

    this.showTripsDate = new Date();
    this.requestedSeatsQty;

    this.loggedOnUser =
    {
        'username': ''
    };

    this.showModal = function () {

        // Open a modal to show the selected user profile
        var modal = Popeye.openModal({
          templateUrl: "JS/Companies/Views/newReservationModal.html",
          //controller: "myModalCtrl as ctrl",
          //resolve: {
          //  user: function($http) {
          //    return $http.get("/users/" + userId);
          //  }
          }
        );

        // Show a spinner while modal is resolving dependencies
        self.showLoading = true;
        modal.resolved.then(function() {
          self.showLoading = false;
        });

        // Update user after modal is closed
        modal.closed.then(function() {
          self.updateUser();
        });
    };

    this.getRoutesFromCompany = function () {

        self.routes = [];

        RoutesService.getRoutesFromCompany(self.company.companyId).then(function(response){

            self.routes = response.data.routes;

        });
    };

    this.getInitialStops = function (aRoute) {

        StopsService.getAllStops(aRoute.routeId).then(function(response) {

            self.initialStops = [];
            self.finalStops = [];
            self.initialStops = response.data.stops;

        });

    };

    this.getFinalStops = function (aRoute, aStop) {

        StopsService.getNextStops(aRoute.routeId, aStop.stopId).then(function(response) {

            self.finalStops = [];
            self.finalStops = response.data.stops;

        });

    };

    this.getTripsWithRequestedAvailability = function () {

        AvailabilityService.getCompanyTripsByRouteAndDate(self.selectedRoute.routeId, moment(self.showTripsDate).format('YYYY-MM-DD'), self.requestedSeatsQty, self.selectedInitialStop.stopId, self.selectedFinalStop.stopId).then(function(response){

            self.trips = [];
            self.trips = response.data.trips;

        });

    };

    this.login = function () {

        var tokenInfo = localStorageService.get('KWSession');
        self.company.companyId = localStorageService.get('KWCompany');

        if (tokenInfo != null) {

            if (jwtHelper.isTokenExpired(tokenInfo)) {

                self.loggedOnUser.username = '';
                self.loggedOnUser.companies = [];

                return;

            }

            tokenInfo = jwtHelper.decodeToken(tokenInfo);
            self.loggedOnUser.username = tokenInfo['username'];
            self.loggedOnUser.companies = tokenInfo['companies'];

            return;

        }

        self.loggedOnUser.username = '';
        self.loggedOnUser.companies = [];

        return;

    };

    this.logout = function () {

        store.remove('token');
        self.loggedOnUser = {};
        $state.go('site.home');

    };

    self.login();
    self.getRoutesFromCompany();

}]);
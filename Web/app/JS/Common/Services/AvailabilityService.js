var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('AvailabilityService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getAvailabilityByTripAndDate = function(routeId, tripDateTime) {

        var url = CONFIG.APIURL + "both/trips/availability",

            promise = $http.get(url + '/routeId/' + routeId + '/dateTime/' + tripDateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getCompanyTripsByRouteAndDate = function(routeId, aDate, qty, initStopId, finalStopId) {

        var url = CONFIG.APIURL + "both/trips",

            promise = $http.get(url + '/routeId/' + routeId + '/date/' + aDate + '/seats/' + qty + '/from/' + initStopId + '/to/' + finalStopId);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
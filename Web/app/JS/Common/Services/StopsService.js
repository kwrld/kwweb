var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('StopsService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getAllStops = function(routeId) {

        var url = CONFIG.APIURL + "both/stops",

            promise = $http.get(url + '/route/' + routeId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getNextStops = function (routeId, stopId) {

        var url = CONFIG.APIURL + "both/stops",

            promise = $http.get(url + '/route/' + routeId + '/stop/' + stopId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getNearbyStops = function (latitude, longitude, radius) {

        var url = CONFIG.APIURL + "both/stops",

            promise = $http.get(url + '/latitude/' + latitude + '/longitude/' + longitude + '/kms/' + radius);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createStop = function (aStop) {

        var url = CONFIG.APIURL + "web/stops/",

            promise = $http.post(url, aStop);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateStop = function (aStop) {

        var url = CONFIG.APIURL + "web/stops/",

            promise = $http.put(url, aStop);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteStop = function (routeId, stopId) {

        var url = CONFIG.APIURL + "web/stops/",

            promise = $http.put(url + 'route/' + routeId + '/stop/' + stopId);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
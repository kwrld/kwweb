var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service("TicketsService", function ($http) {

    this.getUserTickets = function(userId, dateTime, qty) {

        var url = mainUrl + "both/tickets",

            promise = $http.get(url + '/user/' + userId + '/dateTime/' + dateTime + '/qty/' + qty);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getTicketsByDate = function (companyId, dateTime) {

        var url = mainUrl + "web/tickets",

            promise = $http.get(url + '/company/' + companyId + '/date/' + dateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getTicketsByTrip = function (companyId, routeId, aDateTime) {

        var url = mainUrl + "web/tickets",

            promise = $http.get(url + '/company/' + companyId + '/route/' + routeId + '/dateTime/' + aDateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createTicket = function (aTicket) {

        var url = mainUrl + "both/tickets/",

            promise = $http.post(url, aTicket);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateTicket = function (aTicket) {

        var url = mainUrl + "both/tickets/",

            promise = $http.put(url, aTicket);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteTicket = function (routeId, userId, dateTime) {

        var url = mainUrl + "both/tickets",

            promise = $http.put(url + '/route/' + routeId + '/userId/' + userId + '/dateTime/' + dateTime);

        return promise.then(function (response) {

            return response;

        });
    };
});
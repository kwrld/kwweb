var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('CompaniesService', ['CONFIG', '$http', function ( CONFIG, $http) {

    this.getCompanies = function() {

        var url = CONFIG.APIURL + "/both/companies/",

            promise = $http.get(url);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getCompanyById = function (companyId) {

        var url = CONFIG.APIURL + "/both/companies",

            promise = $http.get(url + '/' + companyId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getCompanyByName = function (companyName) {

        var url = CONFIG.APIURL + "/both/companies/",

            promise = $http.get(url + companyName);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createCompany = function (aCompany) {

        var url = CONFIG.APIURL + "/web/companies/",

            promise = $http.post(url, aCompany);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateCompany = function (aCompany) {

        var url = CONFIG.APIURL + "/web/companies/",

            promise = $http.put(url, aCompany);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteCompany = function (companyId) {

        var url = CONFIG.APIURL + "/web/companies/",

            promise = $http.put(url + 'company/' + companyId);

        return promise.then(function (response) {

            return response;

        });
    };

/*
    this.sendRegisterMail = function (company) {

        var url = CONFIG.APIURL + "/both/companies/enviarMail",

            promise = $http.post(url, company);

        return promise.then(function (response) {

            return response;

        });
    };
*/
}]);
var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('UsersService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getUserByName = function(username) {

        var url = CONFIG.APIURL + "both/users/username/",

            promise = $http.get(url + username);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateUser = function (aUser) {

        var url = CONFIG.APIURL + "both/users/",

            promise = $http.put(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createUser = function (aUser) {

        var url = CONFIG.APIURL + "both/users/",

            promise = $http.post(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteUser = function (username) {

        var url = CONFIG.APIURL + "both/users/",

            promise = $http.put(url + username);

        return promise.then(function (response) {

            return response;

        });
    };

    this.userLogin = function (aUser) {

        var url = CONFIG.APIURL + "auth/login",

            promise = $http.post(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.userPasswordReset = function (username, dni) {

        var url = CONFIG.APIURL + "auth/passwordReset",

            promise = $http.post(url + '/user/' + username + '/dni/' + dni);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('RoutesService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getRoutesFromCompany = function(companyId) {

        var url = CONFIG.APIURL + "both/routes",

            promise = $http.get(url + '/company/' + companyId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getRoute = function (routeId) {

        var url = CONFIG.APIURL + "both/routes/",

            promise = $http.get(url + routeId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createRoute = function (aRoute) {

        var url = CONFIG.APIURL + "web/routes/",

            promise = $http.post(url, aRoute);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateRoute = function (aRoute) {

        var url = CONFIG.APIURL + "web/routes/",

            promise = $http.put(url, aRoute);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteRoute = function (routeId) {

        var url = CONFIG.APIURL + "web/routes/",

            promise = $http.put(url + routeId);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
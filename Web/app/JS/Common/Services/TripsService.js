var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('TripsService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getRouteTripsByDateTime = function(routeId, dateTime) {

        var url = CONFIG.APIURL + "both/trips",

            promise = $http.get(url + '/routeId/' + routeId + '/dateTime/' + dateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getRouteTripsByDate = function(routeId, aDate) {

        var url = CONFIG.APIURL + "both/trips",

            promise = $http.get(url + '/routeId/' + routeId + '/date/' + aDate);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getAllTripsByDate = function(aDate) {

        var url = CONFIG.APIURL + "web/trips",

            promise = $http.get(url + '/date/' + aDate);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getAllTripsFromDateTime = function(aDateTime) {

        var url = CONFIG.APIURL + "web/trips",

            promise = $http.get(url + '/dateTime/' + aDateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getRouteTripsBetweenDates = function(routeId, fromDate, toDate) {

        var url = CONFIG.APIURL + "web/trips",

            promise = $http.get(url + '/routeId/' + routeId + '/fromDateTime/' + fromDate + '/toDateTime/' + toDate);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createTrip = function (aTrip) {

        var url = CONFIG.APIURL + "web/trips/",

            promise = $http.post(url, aTrip);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteTrip = function (routeId, aDateTime) {

        var url = CONFIG.APIURL + "web/trips",

            promise = $http.put(url + '/route/' + routeId + '/dateTime/' + aDateTime);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateTrip = function (aTrip) {

        var url = CONFIG.APIURL + "web/trips/",

            promise = $http.put(url, unViaje);

        return promise.then(function (response) {

            return response;

        });
    };

    this.massiveTripsCreation = function (tripConfig) {

        var url = CONFIG.APIURL + "web/trips/schedule",

            promise = $http.post(url, tripConfig);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
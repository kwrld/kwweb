var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('CompanyUsersService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.getUserByName = function(username) {

        var url = CONFIG.APIURL + "web/companiesUsers/username/",

            promise = $http.get(url + username);

        return promise.then(function (response) {

            return response;

        });
    };

    this.getUserById = function(userId) {

        var url = CONFIG.APIURL + "web/companiesUsers/userID/",

            promise = $http.get(url + userId);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateUser = function (aUser) {

        var url = CONFIG.APIURL + "web/companiesUsers/",

            promise = $http.put(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteUser = function (username) {

        var url = CONFIG.APIURL + "web/companiesUsers/",

            promise = $http.put(url + username);

        return promise.then(function (response) {

            return response;

        });
    };

    this.createUser = function (aUser) {

        var url = CONFIG.APIURL + "web/companiesUsers/",

            promise = $http.post(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.userLogin = function (aUser) {

        var url = CONFIG.APIURL + "web/companiesUsers/login",

            promise = $http.post(url, aUser);

        return promise.then(function (response) {

            return response;

        });
    };

    this.userPasswordReset = function (username) {

        var url = CONFIG.APIURL + "web/companiesUsers/passwordReset",

            promise = $http.post(url + username);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
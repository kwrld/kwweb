var KombiWorldApp = angular.module('KombiWorldApp');

KombiWorldApp.service('CustomersService', ['CONFIG', '$http', function (CONFIG, $http) {

    this.createCustomer = function (aCustomer) {

        var url = CONFIG.APIURL + "both/customers/",

            promise = $http.post(url, aPerson);

        return promise.then(function (response) {

            return response;

        });
    };

    this.updateCustomer = function (aCustomer) {

        var url = CONFIG.APIURL + "both/customers/",

            promise = $http.put(url, aPerson);

        return promise.then(function (response) {

            return response;

        });
    };

    this.deleteCustomer = function (dni) {

        var url = CONFIG.APIURL + "web/customers/",

            promise = $http.put(url + dni);

        return promise.then(function (response) {

            return response;

        });
    };
}]);
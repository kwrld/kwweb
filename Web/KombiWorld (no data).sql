SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `kombiworld` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `kombiworld`;

DELIMITER $$
DROP PROCEDURE IF EXISTS `add_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_company` (IN `cuit` VARCHAR(45), IN `aLegalName` VARCHAR(45), IN `aFantasyName` VARCHAR(45), IN `aTelephone` VARCHAR(45), IN `anEmail` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN

    INSERT INTO Companies (companyCuit,
                            companyLegalName,
                            companyFantasyName,
                            telephone,
                            mail,
                            enabled)

    VALUES (cuit,
              aLegalName,
              aFantasyName,
              aTelephone,
              anEmail,
              1);

    SELECT LAST_INSERT_ID() AS "companyId";
  END$$

DROP PROCEDURE IF EXISTS `add_company_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_company_user` (IN `username` VARCHAR(45), IN `dni` INT(11), IN `companyId` INT, IN `pass` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    INSERT INTO CompanyUsers (username,
                                customerDni,
                                companyId,
                                password,
                                enabled)
    VALUES (username,
            dni,
            companyId,
            pass,
            1)
    ON DUPLICATE KEY UPDATE enabled = 1;

    SELECT u.userId,
         u.username,
         u.companyId,
         u.customerDni,
       p.firstName,
           p.lastName,
           p.birthdate,
           p.gender,
           p.telephone,
           p.mail,
           p.enabled
    FROM Customers AS p INNER JOIN CompanyUsers AS u ON
        (u.customerDni = dni AND
          u.customerDni = p.customerDni);
  END$$

DROP PROCEDURE IF EXISTS `add_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_customer` (IN `customerDni` INT, IN `aFantasyName` VARCHAR(45), IN `aLastName` VARCHAR(45), IN `aBirthdate` DATE, IN `aGender` CHAR(1), IN `aTelephone` VARCHAR(45), IN `anEmail` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    INSERT INTO Customers (customerDni,
                            firstName,
                            lastName,
                            birthdate,
                            gender,
                            telephone,
                            mail,
                            enabled)
    VALUES (customerDni,
              aFantasyName,
              aLastName,
              aBirthdate,
              aGender,
              aTelephone,
              anEmail,
              1);

    SELECT customerDni,
          firstName,
              lastName,
              birthdate,
              gender,
              telephone,
              mail,
              enabled
      FROM Customers
      WHERE customerDni = customerDni;
  END$$

DROP PROCEDURE IF EXISTS `add_route`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_route` (IN `companyId` INT, IN `aName` VARCHAR(45) CHARSET utf8, IN `aBaseRate` INT)  SQL SECURITY INVOKER
BEGIN

    INSERT INTO Routes (companyId,
                        name,
                        baseRate,
                        enabled)
    VALUES (companyID,
            aName,
            aBaseRate,
            1)
    ON DUPLICATE KEY UPDATE name = aName,
                          baseRate = aBaseRate,
                          enabled = 1;

  SELECT LAST_INSERT_ID();
  END$$

DROP PROCEDURE IF EXISTS `add_stop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_stop` (IN `aRouteId` INT, IN `aFantasyName` VARCHAR(45), IN `anAddress` VARCHAR(45), IN `aLatitude` VARCHAR(45), IN `aLongitude` VARCHAR(45), IN `anAddicTime` INT, IN `anAddicPrice` INT)  SQL SECURITY INVOKER
BEGIN

    DECLARE autoID INT;

    SELECT MAX(stopId) INTO autoID
      FROM Stops
      WHERE routeId = aRouteId;

    IF autoID IS NULL THEN
        SET autoID := 1;
      ELSE
        SET autoID := autoID + 1;
      END IF;

    INSERT INTO Stops (stopId,
                           routeId,
                           stopName,
                           stopAddress,
                           latitude,
                           longitude,
                           stopElapsedTime,
                           stopAdditionalFee,
                           enabled)
    VALUES (autoID,
              aRouteId,
              aFantasyName,
              anAddress,
              aLatitude,
              aLongitude,
              anAddicTime,
              anAddicPrice,
              1)
    ON DUPLICATE KEY UPDATE stopName = aFantasyName,
                              latitude = aLatitude,
                              longitude = aLongitude,
                              stopAddress = anAddress,
                              stopElapsedTime = anAddicTime,
                              stopAdditionalFee = anAddicPrice,
                              enabled = 1;

    SELECT stopId,
          routeId,
          stopName,
          stopAddress,
          latitude,
          longitude,
          stopElapsedTime,
          stopAdditionalFee,
          enabled
  FROM Stops
  WHERE routeId = aRouteId AND
          stopId = autoID;
  END$$

DROP PROCEDURE IF EXISTS `add_ticket`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_ticket` (IN `aUserId` BIGINT, IN `aRouteId` INT, IN `aTripDateTime` DATETIME, IN `aFirstStopId` INT, IN `aLastStopId` TINYINT, IN `aChannel` ENUM('WEB','MOB'), IN `aQty` INT, IN `aPrice` INT)  SQL SECURITY INVOKER
BEGIN
    INSERT INTO Tickets (userId,
                         routeId,
                         tripDateTime,
                         firstStopId,
                         lastStopId,
                         incomeChannel,
                         numberOfSeats,
                         ticketTotalPrice,
                         enabled)
      VALUES (aUserId,
              aRouteId,
              aTripDateTime,
              aFirstStopId,
              aLastStopId,
              aChannel,
              aQty,
              aPrice,
              1);
  END$$

DROP PROCEDURE IF EXISTS `add_trip`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_trip` (IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    INSERT INTO Trips (routeId,
                        tripDateTime,
                        enabled)
      VALUES (aRouteId,
              aDateTime,
              1)
    ON DUPLICATE KEY UPDATE enabled = 1;

  CALL massive_availability_generation (aRouteId, aDateTime);
  END$$

DROP PROCEDURE IF EXISTS `add_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_user` (IN `dni` INT, IN `aUsername` VARCHAR(45), IN `pass` VARCHAR(255))  SQL SECURITY INVOKER
BEGIN
    INSERT INTO Users (username,
                      customerDni,
                      password,
                      enabled)
    VALUES (aUsername,
            dni,
            pass,
            1)
    ON DUPLICATE KEY UPDATE enabled = 1;

    SELECT u.username,
          p.customerDni,
          p.firstName,
          p.lastName,
          p.birthdate,
          p.gender,
          p.telephone,
          p.mail,
          p.enabled
    FROM Customers AS p INNER JOIN Users AS u ON
        p.customerDni = u.customerDni AND
          p.customerDni = dni;
  END$$

DROP PROCEDURE IF EXISTS `company_cuit_validation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `company_cuit_validation` (IN `cuit` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    SELECT COUNT(1) AS 'Exists'
      FROM Companies
      WHERE companyCuit LIKE ('%' + cuit + '%');
  END$$

DROP PROCEDURE IF EXISTS `company_user_password_reset`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `company_user_password_reset` (IN `aUsername` VARCHAR(45), IN `dni` INT)  SQL SECURITY INVOKER
BEGIN

  /*
    Si este usuario existe y el DNI coincide, avanzamos.

    SELECT userId,
          username,
              customerDni
      FROM CompanyUsers
      WHERE userId = username AND
          customerDni = dni;

    Para el caso, cuando tengamos internet vamos a averiguar cómo hacerlo. Provisoriamente, cuando se ejecute este SP, vamos a crear una password por default, actualizamos el usuario con la misma, y la devolvemos en los resultados.
  */
    UPDATE CompanyUsers
      SET password = "TODOJUNTOYENMAYUSCULAS",
        enabled = 1
      WHERE username = aUsername AND
          customerDni = dni;

    SELECT userId,
          username,
              customerDni,
              password
    FROM CompanyUsers
      WHERE username = aUsername AND
          customerDni = dni;
  END$$

DROP PROCEDURE IF EXISTS `del_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_company` (IN `aCompanyId` INT)  SQL SECURITY INVOKER
BEGIN

  DECLARE done INT DEFAULT FALSE;
  DECLARE compId, rtId INT;
  DECLARE routesCursor CURSOR FOR SELECT companyId, routeId FROM companyRoutes;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  CREATE TEMPORARY TABLE companyRoutes SELECT Rt.companyId, Rt.routeId
                                      FROM Routes AS Rt INNER JOIN Companies AS Cp
                                      ON Cp.companyId = Rt.companyId;

    UPDATE Companies
      SET enabled = 0
      WHERE companyId = aCompanyId;

  WHILE (!done) DO

  OPEN routesCursor;

  read_loop: LOOP
  FETCH routesCursor INTO compId, rtId;

    IF done THEN
      LEAVE read_loop;
    END IF;

    CALL del_route (rtId);

  END LOOP;
  END WHILE;

  CLOSE routesCursor;

    SELECT companyId,
            companyCuit,
            companyLegalName,
            companyFantasyName,
            companyUpDate,
            telephone,
            mail,
            enabled
    FROM Companies
      WHERE companyId = aCompanyId;
  END$$

DROP PROCEDURE IF EXISTS `del_company_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_company_user` (IN `username` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    UPDATE CompanyUsers
      SET enabled = 0
      WHERE username = username;

    SELECT userId, username, userUpDate, enabled
    FROM CompanyUsers
    WHERE username = username;
  END$$

DROP PROCEDURE IF EXISTS `del_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_customer` (IN `dni` INT)  SQL SECURITY INVOKER
BEGIN
    UPDATE Customers
      SET enabled = 0
      WHERE customerDni = dni;

    SELECT customerDni,
            firstName,
            lastName,
            birthdate,
            gender,
            telephone,
            mail,
            enabled
    FROM Customers
    WHERE customerDni = dni;
  END$$

DROP PROCEDURE IF EXISTS `del_pending_reservation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_pending_reservation` (IN `aUserId` INT, IN `aRouteId` INT, IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN

  START TRANSACTION;

  UPDATE Availability AS A, PendingReservations AS PR
  SET A.availability = (A.availability + PR.reqSeats)
  WHERE PR.routeId = aRouteId AND
        PR.userId = aUserId AND
        A.routeId = PR.routeId AND
        PR.tripDateTime = aTripDateTime AND
        A.tripDateTime = PR.tripDateTime AND
        A.stopId BETWEEN PR.firstStopId AND PR.lastStopId;

  DELETE FROM PendingReservations
    WHERE userId = aUserId AND
          routeId = aRouteId AND
          tripDateTime = aTripDateTime;

  COMMIT;

END$$

DROP PROCEDURE IF EXISTS `del_route`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_route` (IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN
  UPDATE Routes
    SET enabled = 0
    WHERE routeId = aRouteId;

  CALL del_route_stops (aRouteId);
    CALL del_route_trips (aRouteId);

  SELECT companyId,
          routeId,
          name,
          baseRate,
          enabled
  FROM Routes
  WHERE routeId = aRouteId;
 END$$

DROP PROCEDURE IF EXISTS `del_route_stops`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_route_stops` (IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN
  UPDATE Stops
    SET enabled = 0
    WHERE routeId = aRouteId;

  UPDATE Availability
  SET availability = 0, enabled = 0
  WHERE routeId = aRouteId;

  SELECT routeId,
          stopId,
          stopName,
          stopAddress,
          longitude,
          latitude,
          stopElapsedTime,
          stopAdditionalFee,
          enabled
  FROM Stops
  WHERE routeId = aRouteId AND enabled;
 END$$

DROP PROCEDURE IF EXISTS `del_stop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_stop` (IN `aRouteId` INT, IN `aStopId` INT)  SQL SECURITY INVOKER
BEGIN
  UPDATE Stops
    SET enabled = 0
    WHERE stopId = aStopId AND
        routeId = aRouteId;

  UPDATE Availability
  SET availability = 0, enabled = 0
  WHERE routeId = aRouteId AND
        stopId = aStopId;

  SELECT routeId,
          stopId,
          stopName,
          stopAddress,
          longitude,
          latitude,
          stopElapsedTime,
          stopAdditionalFee,
          enabled
  FROM Stops
  WHERE routeId = aRouteId AND
        stopId = aStopId AND
        enabled;
 END$$

DROP PROCEDURE IF EXISTS `del_ticket`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_ticket` (IN `aUserId` BIGINT, IN `aCompanyId` INT, IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    UPDATE Tickets
      SET enabled = 0
      WHERE userId = aUserId AND
              routeId = aRouteId AND
              tripDateTime = aDateTime;
    CALL get_ticket(aUserId, aRouteId, aDateTime);
  END$$

DROP PROCEDURE IF EXISTS `del_trip`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_trip` (IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    UPDATE Trips
      SET enabled = 0
      WHERE routeId = aRouteId AND tripDateTime = aDateTime;
    SELECT routeId, tripDateTime, enabled
      FROM Trips
      WHERE routeId = aRouteId AND tripDateTime = aDateTime;
  END$$

DROP PROCEDURE IF EXISTS `del_trip_by_route`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_trip_by_route` (IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN

  UPDATE Availability
  SET availability = 0, enabled = 0
  WHERE routeId = aRouteId;

    UPDATE Trips
  SET enabled = 0
  WHERE routeId = aRouteId;

END$$

DROP PROCEDURE IF EXISTS `del_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `del_user` (IN `aUsername` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    UPDATE Users
      SET enabled = 0
      WHERE username = aUsername;

    SELECT userId, username, userUpDate, enabled
    FROM Users
    WHERE username = aUsername;
  END$$

DROP PROCEDURE IF EXISTS `get_all_trips_from_datetime`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_trips_from_datetime` (IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT id_company_trip,
            routeId,
            tripDateTime
    FROM Trips
      WHERE (aTripDateTime <= tripDateTime) AND enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_all_trips_in_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_trips_in_date` (IN `aDate` DATE)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
            tripDateTime
    FROM Trips
      WHERE aDate = (SELECT DATE(tripDateTime)) AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_available_trips_by_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_available_trips_by_date` (IN `aRouteId` INT, IN `aTripDate` DATE, IN `aFirstStopId` INT, IN `aLastStopId` INT, IN `aSeatsQty` INT)  SQL SECURITY INVOKER
BEGIN

    SELECT T.tripDateTime, T.routeId
    FROM Trips AS T INNER JOIN Availability AS A ON
    T.routeId = aRouteId AND
    A.routeId = T.routeId AND
    (SELECT DATE (T.tripDateTime)) = (SELECT DATE (aTripDate)) AND
    A.tripDateTime = T.tripDateTime AND
    T.enabled AND
    A.enabled AND NOT EXISTS
        (SELECT tripDateTime, routeId
           FROM Availability
             WHERE tripDateTime = T.tripDateTime AND
                 stopId BETWEEN aFirstStopId AND aLastStopId AND
                 routeId = T.routeId AND
                 availability < aSeatsQty AND
                 enabled
        )
    GROUP BY T.tripDateTime, T.routeId;

END$$

DROP PROCEDURE IF EXISTS `get_companies`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_companies` ()  SQL SECURITY INVOKER
BEGIN
    SELECT companyId,
            companyCuit,
            companyLegalName,
            companyFantasyName,
            companyUpDate,
            telephone,
            mail
    FROM Companies
      WHERE enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_companies_by_fantasy_name`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_companies_by_fantasy_name` (IN `aFantasyName` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    SELECT companyId,
            companyCuit,
            companyFantasyName,
            companyLegalName,
            telephone,
            mail,
            enabled
    FROM Companies
    WHERE companyFantasyName LIKE CONCAT('%', aFantasyName ,'%');
  END$$

DROP PROCEDURE IF EXISTS `get_companies_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_companies_user` (IN `aUserId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT Comp.companyId, Comp.companyFantasyName
      FROM Users AS US
      INNER JOIN CompanyUsers AS UE
      ON aUserId = US.userId AND
      US.userId = UE.userId AND
      US.enabled = 1 AND
      UE.enabled = 1
      INNER JOIN Companies AS Emp ON
      UE.companyId = Comp.companyId AND
      Comp.enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company` (IN `aCompanyId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT companyId,
            companyCuit,
            companyLegalName,
            companyFantasyName,
            companyUpDate,
            telephone,
            mail
    FROM Companies
      WHERE companyId = aCompanyId AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_company_by_cuit`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_by_cuit` (IN `cuit` VARCHAR(50))  SQL SECURITY INVOKER
BEGIN

    SELECT COUNT(companyId) AS 'quantity'
      FROM Companies
      WHERE CUIT_Empresa = cuit COLLATE utf8_unicode_ci;

  END$$

DROP PROCEDURE IF EXISTS `get_company_tickets_by_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_tickets_by_date` (IN `companyId` INT, IN `aTicketDate` DATE)  SQL SECURITY INVOKER
BEGIN

    SELECT userId,
              routeId,
              tripDateTime,
              firstStopId,
              lastStopId,
              incomeChannel,
              ticketDateTime,
              numberOfSeats,
              ticketTotalPrice
    FROM Tickets AS P INNER JOIN Routes as R
    ON R.companyId = companyId AND
      R.routeId = P.routeId AND
      (SELECT DATE(P.tripDateTime)) = aTicketDate AND
      R.enabled = 1 AND P.enabled = 1;

  END$$

DROP PROCEDURE IF EXISTS `get_company_trips_route_and_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_trips_route_and_date` (IN `aRouteId` INT, IN `aDate` DATE, IN `aQty` INT, IN `fromStop` INT, IN `toStop` INT)  SQL SECURITY INVOKER
BEGIN

SELECT AvTr.tripDateTime, R.routeId, R.name, S.stopId, S.stopName, AvTr.stopDateTime, AvTr.availability
FROM
(SELECT A.routeId, A.tripDateTime, A.stopId, A.availability, A.stopDateTime
    FROM Availability AS A WHERE
    (SELECT DATE(A.tripDateTime)) = aDate AND
  A.enabled AND
  A.tripDateTime NOT IN (

        (SELECT AV.tripDateTime
         FROM Availability AS AV
         WHERE AV.routeId = aRouteId AND
         AV.tripDateTime = A.tripDateTime
         AND
         AV.stopId BETWEEN fromStop AND toStop AND
         AV.availability < aQty AND
         AV.enabled
        )
    )
) AS AvTr INNER JOIN Routes AS R ON
AvTr.routeId = R.routeId AND
R.enabled INNER JOIN Stops AS S ON
S.stopId = AvTr.stopId AND
S.routeId = AvTr.routeId AND
S.enabled;

END$$

DROP PROCEDURE IF EXISTS `get_company_user_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_user_by_id` (IN `aUserId` INT(11))  SQL SECURITY INVOKER
BEGIN
    SELECT userId,
          username,
          customerDni,
          enabled
    FROM CompanyUsers
      WHERE userId = aUserId AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_company_user_by_username`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_company_user_by_username` (IN `aUsername` VARCHAR(255))  SQL SECURITY INVOKER
BEGIN
    SELECT username,
          customerDni,
          userUpDate,
          password
    FROM CompanyUsers
      WHERE enabled = 1 AND
          username = aUsername;
  END$$

DROP PROCEDURE IF EXISTS `get_customer`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_customer` (IN `customerDni` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT customerId,
          firstName,
          lastName,
          birthdate,
          gender,
          telephone,
          email
    FROM Customers
      WHERE customerId = customerDni AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_customers`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_customers` ()  SQL SECURITY INVOKER
BEGIN
    SELECT customerDni,
          firstName,
          lastName,
          birthdate,
          gender,
          telephone,
          mail
    FROM Customers
      WHERE enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_customer_by_username`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_customer_by_username` (IN `aUsername` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
  SELECT C.customerId,
          firstName,
          lastName,
          birthdate,
          gender,
          telephone,
          email
      FROM Users as U INNER JOIN Customers AS C ON
          U.username = aUsername AND
          U.customerId = C.customerId AND
          U.enabled AND
          C.enabled;
  END$$

DROP PROCEDURE IF EXISTS `get_historical_user_tickets`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_historical_user_tickets` (IN `aUserId` BIGINT)  SQL SECURITY INVOKER
BEGIN

  SELECT C.companyFantasyName AS 'companyName',
      C.companyId,
        R.name AS 'routeName',
        R.routeId,
        H.tripDateTime,
        H.ticketDateTime,
        H.incomeChannel,
        H.numberOfSeats,
        H.ticketTotalPrice,
        FS.stopName AS 'fromStopName',
        FS.stopId AS 'fromStopId',
        FS.latitude AS 'fromStopLat',
        FS.longitude AS 'fromStopLon',
        FS.stopAddress AS 'fromStopAddress',
        TS.stopName AS 'toStopName',
        TS.stopId AS 'toStopId',
        TS.latitude AS 'toStopLat',
        TS.longitude AS 'toStopLon',
        TS.stopAddress AS 'toStopAddress'
    FROM Users AS U INNER JOIN HistoricalUserTickets AS H ON
    U.userId = aUserId AND
    U.enabled AND
    U.userId = H.userId INNER JOIN Routes AS R ON
    R.routeId = H.routeId AND
    R.enabled INNER JOIN Companies AS C ON
    R.companyId = C.companyId AND
    C.enabled INNER JOIN Stops AS FS ON
    FS.stopId = H.firstStopId AND
    FS.routeId = H.routeId AND
    FS.enabled INNER JOIN Stops AS TS ON
    TS.stopId = H.lastStopId AND
    TS.routeId = H.routeId AND
    TS.enabled;

END$$

DROP PROCEDURE IF EXISTS `get_nearby_stops`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_nearby_stops` (IN `fromLat` FLOAT(10,6), IN `fromLon` FLOAT(10,6), IN `toLat` FLOAT(10,6), IN `toLon` FLOAT(10,6), IN `radius` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT fromStops.stopId AS 'fromStopId',
            fromStops.routeId AS 'fromStopRouteId',
            fromStops.stopName AS 'fromStopName',
            fromStops.stopAddress AS 'fromStopAddress',
            fromStops.latitude AS 'fromStopLat',
            fromStops.longitude AS 'fromStopLon',
            R.routeId AS 'routeId',
            R.name AS 'routeName',
            R.baseRate AS 'routeBaseRate',
            C.companyId AS 'companyId',
            C.companyFantasyName AS 'companyName',
            toStops.stopId AS 'toStopId',
            toStops.routeId AS 'toRouteId',
            toStops.stopName AS 'toStopName',
            toStops.stopAddress AS 'toStopAddress',
            toStops.latitude AS 'toStopLat',
            toStops.longitude AS 'toStopLon'
    FROM
        (
           SELECT  stopId, routeId, stopName, stopAddress, latitude, longitude,
                  (6371 * acos(cos(radians(latitude)) *
                               cos(radians(fromLat)) *
                               cos(radians(fromLon) -
                                   radians(longitude)) +
                               sin(radians(latitude)) *
                               sin(radians(fromLat)))) AS distance
          FROM Stops
          WHERE enabled
          HAVING distance <= radius
          ORDER BY distance
        ) AS fromStops INNER JOIN
        (
           SELECT  stopId, routeId, stopName, stopAddress, latitude, longitude,
                  (6371 * acos(cos(radians(latitude)) *
                               cos(radians(toLat)) *
                               cos(radians(toLon) -
                                   radians(longitude)) +
                               sin(radians(latitude)) *
                               sin(radians(toLat)))) AS distance
          FROM Stops
          WHERE enabled
          HAVING distance <= radius
          ORDER BY distance
        ) AS toStops ON
        fromStops.routeId = toStops.routeId AND
        fromStops.stopId < toStops.stopId INNER JOIN Routes AS R ON
        fromStops.routeId = R.routeId AND R.enabled INNER JOIN
        Companies AS C ON
        R.companyId = C.companyId AND
        C.enabled
        GROUP BY fromStops.routeId;
END$$

DROP PROCEDURE IF EXISTS `get_pending_reservation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_pending_reservation` (IN `aUserId` INT, IN `aRouteId` INT, IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN

  SELECT userId, routeId, tripDateTime, firstStopId, lastStopId, reqSeats, totalPrice, incomeChannel
    FROM PendingReservations
    WHERE userId = aUserId AND
        routeId = aRouteId AND
            tripDateTime = aTripDateTime;

END$$

DROP PROCEDURE IF EXISTS `get_routes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_routes` (IN `aCompanyId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
            companyId,
            name,
            baseRate
    FROM Routes
      WHERE companyId = aCompanyId AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_route_trips_by_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_route_trips_by_date` (IN `aRouteId` INT, IN `aTripDate` DATE)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
            tripDateTime
    FROM Trips
      WHERE routeId = aRouteId AND
            aTripDate = (SELECT DATE(tripDateTime)) AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_stop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stop` (IN `aStopId` INT, IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT stopId,
            routeId,
            stopName,
            latitude,
            longitude
    FROM Stops
      WHERE stopId = aStopId AND
            routeId = aRouteId AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_stops_availability_by_trip`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stops_availability_by_trip` (IN `aRouteId` INT, IN `aTripDateTime` DATETIME, IN `aFirstStop` INT, IN `aLastStop` INT)  SQL SECURITY INVOKER
BEGIN

  SELECT stopId, routeId, tripDateTime, stopDateTime, availability
  FROM Availability
  WHERE routeId = aRouteId AND
      tripDateTime = aTripDateTime AND
          stopId BETWEEN aFirstStop AND aLastStop AND
          enabled;
END$$

DROP PROCEDURE IF EXISTS `get_stops_by_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stops_by_company` (IN `aCompanyId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT stopId,
            routeId,
            stopName,
            latitude,
            longitude
    FROM Stops AS P INNER JOIN Routes AS R ON
          R.companyId = aCompanyId AND
          R.routeId = P.routeId AND
          R.enabled = 1 AND P.enabled = 1;

  END$$

DROP PROCEDURE IF EXISTS `get_stops_by_route`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stops_by_route` (IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT stopId,
            routeId,
            stopName,
            stopAddress,
            latitude,
            longitude,
            stopElapsedTime,
            stopAdditionalFee
    FROM Stops
    WHERE routeId = aRouteId AND
          enabled = 1
    ORDER BY stopId ASC;
END$$

DROP PROCEDURE IF EXISTS `get_stops_from_another_stop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_stops_from_another_stop` (IN `aRouteId` INT, IN `aStopId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT stopId,
            routeId,
            stopName,
            stopAddress,
            latitude,
            longitude,
            stopElapsedTime,
            stopAdditionalFee
    FROM Stops
    WHERE routeId = aRouteId AND
          stopId > aStopId AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_ticket`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_ticket` (IN `aUserId` BIGINT, IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT userId,
           routeId,
           tripDateTime,
           firstStopId,
           lastStopId,
           incomeChannel,
           ticketDateTime,
           numberOfSeats,
           ticketTotalPrice
    FROM Tickets
      WHERE aUserId = userId AND
            routeId = aRouteId AND
            aDateTime = tripDateTime AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_tickets_by_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_by_company` (IN `aCompanyId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT ticketId,
            aFantasyName_user_ticket,
            tripId,
            companyId,
            firstStopId,
            lastStopId,
            incomeChannel
    FROM Tickets AS P INNER JOIN Routes AS R ON
          R.companyId = aCompanyId AND
          R.companyId = P.routeId AND
          P.enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_tickets_by_route_and_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_by_route_and_date` (IN `aRouteId` INT, IN `aTicketDate` DATE)  SQL SECURITY INVOKER
BEGIN

    SELECT userId,
            routeId,
            tripDateTime,
            firstStopId,
            lastStopId,
            incomeChannel,
            ticketDateTime,
            numberOfSeats,
            ticketTotalPrice,
            enabled
      FROM Tickets
      WHERE  routeId = aRouteId AND
            (SELECT DATE(tripDateTime)) = aTicketDate AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_tickets_by_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_tickets_by_user` (IN `aUserId` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    SELECT userId,
           routeId,
           tripDateTime,
           firstStopId,
           lastStopId,
           incomeChannel,
           ticketDateTime,
           numberOfSeats,
           ticketTotalPrice
    FROM Tickets
      WHERE aUserId = userId AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_trip`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_trip` (IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
           tripDateTime
    FROM Trips
    WHERE routeId = aRouteId AND
          tripDateTime = aDateTime AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_trips_between_dates`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_trips_between_dates` (IN `aRouteId` INT, IN `aDateFrom` DATETIME, IN `aDateTo` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
            tripDateTime
    FROM Trips
      WHERE routeId = aRouteId AND
            (aDateFrom <= tripDateTime) AND
            (aDateTo >= tripDateTime) AND
            enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_trips_by_route_and_date`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_trips_by_route_and_date` (IN `aRouteId` INT, IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId, tripDateTime
    FROM Availability
    WHERE routeId = aRouteId AND
    (SELECT DATE(tripDateTime)) = aTripDateTime AND
    enabled
    GROUP BY tripDateTime
    ORDER BY tripDateTime ASC;
END$$

DROP PROCEDURE IF EXISTS `get_trips_route_from_datetime`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_trips_route_from_datetime` (IN `aRouteId` INT, IN `aTicketDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN
    SELECT routeId,
            tripDateTime
    FROM Trips
    WHERE routeId = aRouteId AND
          (aTicketDateTime <= tripDateTime) AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_trip_tickets`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_trip_tickets` (IN `aRouteId` INT, IN `aDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN

    SELECT userId,
            routeId,
            tripDateTime,
            firstStopId,
            lastStopId,
            incomeChannel,
            ticketDateTime,
            numberOfSeats,
            ticketTotalPrice,
            enabled
    FROM Tickets
    WHERE routeId = aRouteId AND
          tripDateTime = aDateTime AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user` (IN `aUsername` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    SELECT username,
          customerDni,
          userUpDate,
          password
    FROM Users
      WHERE enabled = 1 AND
          username = aUsername;
  END$$

DROP PROCEDURE IF EXISTS `get_users`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_users` ()  SQL SECURITY INVOKER
BEGIN
    SELECT username,
          customerDni,
          userUpDate,
          password
    FROM Users
      WHERE enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_user_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_by_id` (IN `aUserId` INT)  SQL SECURITY INVOKER
BEGIN
    SELECT userId,
          username,
          customerDni,
          enabled
    FROM Users
      WHERE userId = aUserId AND
          enabled = 1;
  END$$

DROP PROCEDURE IF EXISTS `get_user_tickets`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_tickets` (IN `aUserId` INT, IN `aDateFrom` DATETIME, IN `qty` INT)  SQL SECURITY INVOKER
BEGIN

    SELECT userId,
            routeId,
            tripDateTime,
            firstStopId,
            lastStopId,
            incomeChannel,
            ticketDateTime,
            numberOfSeats,
            ticketTotalPrice,
            enabled
      FROM Tickets
      WHERE userId = aUserId AND
            tripDateTime > aDateFrom LIMIT qty;
  END$$

DROP PROCEDURE IF EXISTS `login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `login` (IN `aUsername` VARCHAR(50), IN `pass` VARCHAR(255))  SQL SECURITY INVOKER
BEGIN
    SELECT COUNT(userId) AS 'OK',
            userId AS 'aUserId',
            username,
                  password
    FROM Users WHERE username = aUsername COLLATE utf8_unicode_ci AND
                password = pass COLLATE utf8_unicode_ci;
  END$$

DROP PROCEDURE IF EXISTS `login_company_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `login_company_user` (IN `aUsername` VARCHAR(50), IN `pass` VARCHAR(255))  SQL SECURITY INVOKER
BEGIN
    SELECT COUNT(userId) AS 'OK',
            userId AS 'UserId',
            username,
            password
    FROM CompanyUsers
    WHERE username = aUsername AND
        password = pass;
  END$$

DROP PROCEDURE IF EXISTS `massive_availability_generation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `massive_availability_generation` (IN `aRouteId` INT, IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN

  DECLARE done INT DEFAULT FALSE;
  DECLARE varStopId, varRouteId INT;
  DECLARE varStopElapsedTime INT;
  DECLARE aStopDateTime DATETIME;
  DECLARE stopsCursor CURSOR FOR SELECT stopId, routeId, stopElapsedTime FROM stopsAndTrips;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


  DROP TEMPORARY TABLE IF EXISTS stopsAndTrips;

  CREATE TEMPORARY TABLE stopsAndTrips SELECT St.stopId, St.routeId, St.stopElapsedTime
                                        FROM Routes AS Rt INNER JOIN Stops AS St
                                        ON Rt.routeId = aRouteId AND
                                            Rt.routeId = St.routeId AND
                                            Rt.enabled AND
                                            St.enabled;

  SET aStopDateTime = aTripDateTime;

  WHILE (!done) DO

    OPEN stopsCursor;

    read_loop: LOOP
    FETCH stopsCursor INTO varStopId, varRouteId, varStopElapsedTime;

      IF done THEN
        LEAVE read_loop;
      END IF;

      INSERT INTO Availability (stopId, routeId, tripDateTime, stopDateTime, availability, enabled)
      VALUES (varStopId, varRouteId, aTripDateTime, aStopDateTime, 19, 1);

      SET aStopDateTime = (SELECT DATE_ADD(aStopDateTime, INTERVAL varStopElapsedTime MINUTE));

    END LOOP;
  END WHILE;

  CLOSE stopsCursor;

END$$

DROP PROCEDURE IF EXISTS `massive_trip_generation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `massive_trip_generation` (IN `aDateFrom` DATETIME, IN `aDateTo` DATETIME, IN `frequency` INT, IN `onMondays` BOOLEAN, IN `onTuesdays` BOOLEAN, IN `onWednesdays` BOOLEAN, IN `onThursdays` BOOLEAN, IN `onFridays` BOOLEAN, IN `onSaturdays` BOOLEAN, IN `onSundays` BOOLEAN, IN `aRouteId` INT)  SQL SECURITY INVOKER
BEGIN

    DECLARE auxDateTimeFrom DATETIME DEFAULT aDateFrom;
    DECLARE auxDateTimeTo DATETIME DEFAULT aDateTo;
    DECLARE daysCount INT DEFAULT 0;
    DECLARE auxDayOfWeek INT DEFAULT 0;


    IF (SELECT DATE(aDateFrom)) <= (SELECT DATE(aDateTo)) THEN

      IF ((SELECT DATE(aDateFrom)) < (SELECT DATE(aDateTo))) &&
         ((SELECT TIME(aDateFrom)) > (SELECT TIME(aDateTo))) THEN

        SET auxDateTimeTo = CONCAT((SELECT DATE(aDateFrom)), ' ', (SELECT TIME(aDateTo)));
        SET auxDateTimeTo = (SELECT DATE_ADD(auxDateTimeTo, INTERVAL 1 DAY));

      ELSEIF ((SELECT DATE(aDateFrom)) < (SELECT DATE(aDateTo))) &&
             ((SELECT TIME(aDateFrom)) < (SELECT TIME(aDateTo))) THEN

        SET auxDateTimeTo = CONCAT((SELECT DATE(aDateFrom)), ' ', (SELECT TIME(aDateFrom)));

      ELSEIF ((SELECT DATE(aDateFrom)) = (SELECT DATE(aDateTo))) &&
             ((SELECT TIME(aDateFrom)) < (SELECT TIME(aDateTo))) THEN

        SET auxDateTimeTo = aDateTo;

      END IF;

      WHILE (auxDateTimeFrom <= aDateTo) DO

        SET auxDayOfWeek = (SELECT DAYOFWEEK(auxDateTimeFrom));

        IF ((SELECT DATE(aDateFrom)) = (SELECT DATE(aDateTo))) OR
           ((auxDayOfWeek = 1 AND onSundays) OR
            (auxDayOfWeek = 2 AND onMondays) OR
            (auxDayOfWeek = 3 AND onTuesdays) OR
            (auxDayOfWeek = 4 AND onWednesdays) OR
            (auxDayOfWeek = 5 AND onThursdays) OR
            (auxDayOfWeek = 6 AND onFridays) OR
            (auxDayOfWeek = 7 AND onSaturdays)) THEN

          WHILE (auxDateTimeFrom <= auxDateTimeTo) DO

              INSERT INTO Trips (routeId, tripDateTime, enabled)
                VALUES (aRouteId, auxDateTimeFrom, 1);

              CALL massive_availability_generation (aRouteId, auxDateTimeFrom);

              SET auxDateTimeFrom = (SELECT DATE_ADD(auxDateTimeFrom, INTERVAL frequency MINUTE));

          END WHILE;
        END IF;

        SET daysCount = daysCount + 1;

        IF ((SELECT DATE(aDateFrom)) < (SELECT DATE(aDateTo))) &&
           ((SELECT TIME(aDateFrom)) > (SELECT TIME(aDateTo))) THEN

          SET auxDateTimeFrom = (SELECT DATE_ADD(aDateFrom, INTERVAL daysCount DAY));

          SET auxDateTimeTo = CONCAT((SELECT DATE(auxDateTimeFrom)), ' ', (SELECT TIME(aDateTo)));
          SET auxDateTimeTo = (SELECT DATE_ADD(auxDateTimeTo, INTERVAL 1 DAY));

        ELSEIF ((SELECT DATE(aDateFrom)) < (SELECT DATE(aDateTo))) &&
               ((SELECT TIME(aDateFrom)) < (SELECT TIME(aDateTo))) THEN

          SET auxDateTimeFrom = (SELECT DATE_ADD(aDateFrom, INTERVAL daysCount DAY));
          SET auxDateTimeTo = CONCAT((SELECT DATE(auxDateTimeFrom)), ' ', (SELECT TIME(aDateTo)));

        ELSEIF ((SELECT DATE(aDateFrom)) = (SELECT DATE(aDateTo))) &&
               ((SELECT TIME(aDateFrom)) < (SELECT TIME(aDateTo))) THEN

          SET auxDateTimeTo = aDateTo;

        END IF;
      END WHILE;
    END IF;

      SELECT true;
  END$$

DROP PROCEDURE IF EXISTS `reservation_confirmation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `reservation_confirmation` (IN `aUserId` INT, IN `aRouteId` INT, IN `aTripDateTime` DATETIME)  SQL SECURITY INVOKER
BEGIN

  START TRANSACTION;

    INSERT INTO Tickets (userId, routeId, tripDateTime, firstStopId, lastStopId, incomeChannel, numberOfSeats, ticketTotalPrice, enabled)
    (SELECT userId, routeId, tripDateTime, firstStopId, lastStopId, incomeChannel, reqSeats, totalPrice, 1
    FROM PendingReservations
    WHERE userId = aUserId AND
        routeId = aRouteId AND
        tripDateTime = aTripDateTime);

    INSERT INTO HistoricalUserTickets (userId, routeId, tripDateTime, ticketDateTime, incomeChannel, numberOfSeats, ticketTotalPrice, firstStopId, lastStopId)
    (SELECT userId, routeId, tripDateTime, ticketDateTime, incomeChannel, numberOfSeats, ticketTotalPrice, firstStopId, lastStopId
    FROM Tickets
    WHERE userId = aUserId AND
        routeId = aRouteId AND
        tripDateTime = aTripDateTime);

    DELETE FROM PendingReservations
        WHERE userId = aUserId AND
          routeId = aRouteId AND
            tripDateTime = aTripDateTime;

  COMMIT;
END$$

DROP PROCEDURE IF EXISTS `set_pre_reservation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `set_pre_reservation` (IN `aRouteId` INT, IN `aTripDateTime` DATETIME, IN `aUserId` INT, IN `aReqSeats` TINYINT, IN `aFirstStopId` INT, IN `aLastStopId` INT, IN `anIncomeChannel` ENUM('WEB','MOB'))  SQL SECURITY INVOKER
BEGIN

DECLARE totalPrice INT DEFAULT 0;
DECLARE isAvailable INT DEFAULT 0;

  START TRANSACTION;

  SET @totalPrice = (SELECT SUM(R.baseRate + TP.TotalParadas) AS "Total"
            FROM (
              SELECT SUM(S.stopAdditionalFee) AS "TotalParadas", S.routeId
              FROM Stops AS S
              WHERE S.routeId = 1 AND
                  S.enabled AND
                  S.stopId BETWEEN 2 AND 3) AS TP INNER JOIN Routes AS R ON
            R.routeId = TP.routeId AND R.enabled
            GROUP BY R.routeId);

  INSERT INTO PendingReservations (userId, routeId, tripDateTime, firstStopId, lastStopId, reqSeats, totalPrice, incomeChannel)
  VALUES (aUserId, aRouteId, aTripDateTime, aFirstStopId, aLastStopId, aReqSeats, (SELECT @totalPrice), anIncomeChannel);

  SET @isAvailable = (SELECT COUNT(*)
            FROM Trips AS T
            WHERE T.routeId = aRouteId AND
                  T.tripDateTime = aTripDateTime AND
                 T.enabled AND NOT EXISTS
               (SELECT A.tripDateTime, A.routeId
                  FROM Availability AS A
                    WHERE A.tripDateTime = T.tripDateTime AND
                        A.stopId BETWEEN aFirstStopId AND aLastStopId AND
                        A.routeId = T.routeId AND
                        A.availability < aReqSeats AND
                        A.enabled
            ));

  IF (SELECT @isAvailable >= 1) THEN

    UPDATE Availability AS A
    SET availability = (availability - aReqSeats)
    WHERE A.tripDateTime = aTripDateTime AND
          A.routeId = aRouteId AND
          A.stopId BETWEEN aFirstStopId AND aLastStopId AND
          A.enabled;

    COMMIT;
  ELSE
    ROLLBACK;
  END IF;

  SELECT userId, routeId, tripDateTime, firstStopId, lastStopId, reqSeats, totalPrice, incomeChannel
  FROM PendingReservations
  WHERE userId = aUserId AND
      routeId = aRouteId AND
      tripDateTime = aTripDateTime AND
      firstStopId = aFirstStopId AND
      lastStopId = aLastStopId;
END$$

DROP PROCEDURE IF EXISTS `upd_company`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_company` (IN `aCompanyId` INT, IN `cuit` VARCHAR(45), IN `aLegalName` VARCHAR(45), IN `aFantasyName` VARCHAR(45), IN `aTelephone` VARCHAR(45), IN `anEmail` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    UPDATE Companies
      SET companyCuit = cuit,
          companyLegalName = aLegalName,
          companyFantasyName = aFantasyName,
          telephone = aTelephone,
          mail = anEmail,
          enabled = 1
    WHERE companyId = aCompanyId;

    SELECT companyId,
        companyCuit,
          companyLegalName,
          companyFantasyName,
          telephone,
          mail,
          enabled
    FROM Companies
      WHERE companyId = aCompanyId;
  END$$

DROP PROCEDURE IF EXISTS `upd_company_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_company_user` (IN `aUsername` VARCHAR(45), IN `dni` INT)  SQL SECURITY INVOKER
BEGIN
    UPDATE CompanyUsers
    SET password = pass,
        enabled = 1
    WHERE username = aUsername;

    SELECT userId,
          username,
              customerDni,
              enabled
    FROM CompanyUsers
    WHERE username = aUsername;
  END$$

DROP PROCEDURE IF EXISTS `upd_customer_from_mobile`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_customer_from_mobile` (IN `aUserName` VARCHAR(45), IN `aName` VARCHAR(45), IN `aLastName` VARCHAR(45), IN `aBirthdate` DATE, IN `aGender` CHAR(1), IN `aTelephone` VARCHAR(20), IN `anEmail` VARCHAR(60))  SQL SECURITY INVOKER
BEGIN
    UPDATE Customers
      SET firstName = aName,
          lastName = aLastName,
          birthdate = aBirthdate,
          gender = aGender,
          telephone = aTelephone,
          email = anEmail,
          enabled = 1
      WHERE customerId IN (
          SELECT customerId
          FROM Users
          WHERE username = aUsername);


      SELECT C.customerId,
              firstName,
              lastName,
              birthdate,
              gender,
              telephone,
              email
    FROM Customers AS C INNER JOIN Users AS U ON
    U.username = aUserName AND
    C.customerId = U.customerId AND
    C.enabled AND
    U.enabled;
  END$$

DROP PROCEDURE IF EXISTS `upd_customer_from_web`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_customer_from_web` (IN `dni` INT, IN `aName` VARCHAR(45), IN `aLastName` VARCHAR(45), IN `aBirthdate` DATE, IN `aGender` BIT, IN `aTelephone` VARCHAR(45), IN `anEmail` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    UPDATE Customers
      SET firstName = aName,
          lastName = aLastName,
          birthdate = aBirthdate,
          gender = aGender,
          telephone = aTelephone,
          email = anEmail,
          enabled = 1
      WHERE customerId = dni;

      SELECT customerId,
              firstName,
              lastName,
              birthdate,
              gender,
              telephone,
              email
    FROM Customers
      WHERE customerId = dni;
  END$$

DROP PROCEDURE IF EXISTS `upd_route`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_route` (IN `aCompanyId` INT, IN `aRouteId` INT, IN `aName` VARCHAR(45), IN `aBaseFee` INT)  SQL SECURITY INVOKER
BEGIN
    UPDATE Routes
      SET name = aName,
          baseRate = aBaseFee,
          enabled = 1
      WHERE routeId = aRouteId AND
            companyId = aCompanyId;

    SELECT companyId,
            routeId,
            name,
            baseRate,
            enabled
    FROM Routes
    WHERE companyId = aCompanyId AND
        routeId = aRouteId;

  END$$

DROP PROCEDURE IF EXISTS `upd_stop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_stop` (IN `aRouteId` INT, IN `aStopId` INT, IN `aName` VARCHAR(45), IN `anAddress` VARCHAR(45), IN `aLatitude` VARCHAR(45), IN `aLongitude` VARCHAR(45), IN `anAddicTime` INT, IN `anAddicPrice` INT)  SQL SECURITY INVOKER
BEGIN
    UPDATE Stops
    SET stopName = aName,
          stopAddress = anAddress,
          latitude = latitude,
          longitude = longitude,
          stopElapsedTime = anAddicTime,
          stopAdditionalFee = anAddicPrice,
        enabled = 1
        WHERE routeId = aRouteId AND
              stopId = aStopId;

    SELECT routeId,
            stopId,
            stopName,
            stopAddress,
            latitude,
            longitude,
            stopElapsedTime,
            stopAdditionalFee,
            enabled
    FROM Stops
      WHERE routeId = aRouteId AND stopId = aStopId;
  END$$

DROP PROCEDURE IF EXISTS `upd_ticket`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_ticket` (IN `aUserId` BIGINT, IN `aRouteId` INT, IN `aTripDateTime` DATETIME, IN `aFirstStopId` INT, IN `aLastStopId` INT, IN `aQty` SMALLINT, IN `aPrice` INT)  SQL SECURITY INVOKER
BEGIN
    UPDATE Tickets
      SET numberOfSeats = aQty,
          ticketTotalPrice = aPrice,
          firstStopId = aFirstStopId,
          lastStopId = aLastStopId,
          enabled = 1
    WHERE userId = aUserId AND
            routeId = aRouteId AND
            tripDateTime = aTripDateTime;
  END$$

DROP PROCEDURE IF EXISTS `upd_trip`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_trip` (IN `aRouteId` INT, IN `aTripDateTime` INT)  SQL SECURITY INVOKER
BEGIN
      UPDATE Trips
      SET enabled = 1
      WHERE routeId = aRouteId AND tripDateTime = aTripDateTime;

    SELECT routeId,
            tripDateTime,
            enabled
    FROM Trips
      WHERE routeId = aRouteId AND
            tripDateTime = aTripDateTime;
  END$$

DROP PROCEDURE IF EXISTS `upd_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `upd_user` (IN `aUsername` VARCHAR(45), IN `pass` VARCHAR(45))  SQL SECURITY INVOKER
BEGIN
    UPDATE Users
    SET password = pass,
        enabled = 1
    WHERE username = aUsername;

    SELECT userId,
          username,
          customerDni,
          enabled
    FROM Users
    WHERE username = aUsername;
  END$$

DROP PROCEDURE IF EXISTS `user_password_reset`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_password_reset` (IN `aUsername` VARCHAR(45), IN `dni` INT)  SQL SECURITY INVOKER
BEGIN

  /*
    Si este usuario existe y el DNI coincide, avanzamos.

    SELECT userId,
          username,
              customerDni
      FROM Users
      WHERE userId = username AND
          customerDni = dni;

    Para el caso, cuando tengamos internet vamos a averiguar cómo hacerlo. Provisoriamente, cuando se ejecute este SP, vamos a crear una password por default, actualizamos el usuario con la misma, y la devolvemos en los resultados.
  */
    UPDATE Users
      SET password = "TODOJUNTOYENMAYUSCULAS",
        enabled = 1
      WHERE username = aUsername AND
          customerDni = dni;

    SELECT userId,
          username,
              customerDni,
              password
    FROM Users
      WHERE username = aUsername AND
          customerDni = dni;
  END$$

DELIMITER ;

DROP TABLE IF EXISTS `Availability`;
CREATE TABLE IF NOT EXISTS `Availability` (
  `stopId` int(11) NOT NULL,
  `routeId` int(11) NOT NULL,
  `tripDateTime` datetime NOT NULL,
  `stopDateTime` datetime NOT NULL,
  `availability` int(11) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`stopId`,`routeId`,`tripDateTime`),
  KEY `trip_availability` (`routeId`,`tripDateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Companies`;
CREATE TABLE IF NOT EXISTS `Companies` (
  `companyId` int(11) NOT NULL AUTO_INCREMENT,
  `companyCuit` varchar(45) CHARACTER SET utf8 DEFAULT '',
  `companyLegalName` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `companyFantasyName` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `companyUpDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `telephone` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `mail` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `mailVerified` tinyint(1) DEFAULT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`companyId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `CompanyUsers`;
CREATE TABLE IF NOT EXISTS `CompanyUsers` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) NOT NULL,
  `customerDni` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `userUpDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`userId`),
  KEY `customer_company` (`companyId`),
  KEY `companyUser_customer` (`customerDni`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Customers`;
CREATE TABLE IF NOT EXISTS `Customers` (
  `customerId` int(11) NOT NULL,
  `firstName` varchar(45) CHARACTER SET utf8 NOT NULL,
  `lastName` varchar(45) CHARACTER SET utf8 NOT NULL,
  `birthdate` datetime NOT NULL,
  `telephone` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `gender` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`customerId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `HistoricalUserTickets`;
CREATE TABLE IF NOT EXISTS `HistoricalUserTickets` (
  `userId` bigint(11) NOT NULL,
  `routeId` int(11) NOT NULL,
  `tripDateTime` datetime NOT NULL,
  `firstStopId` int(11) NOT NULL,
  `lastStopId` int(11) NOT NULL,
  `ticketDateTime` datetime NOT NULL,
  `incomeChannel` enum('WEB','MOB') COLLATE utf8_spanish_ci NOT NULL,
  `numberOfSeats` tinyint(4) NOT NULL,
  `ticketTotalPrice` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`routeId`,`tripDateTime`),
  KEY `history_route` (`routeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `PendingReservations`;
CREATE TABLE IF NOT EXISTS `PendingReservations` (
  `userId` bigint(20) NOT NULL,
  `routeId` int(11) NOT NULL,
  `tripDateTime` datetime NOT NULL,
  `firstStopId` int(11) NOT NULL,
  `lastStopId` int(11) NOT NULL,
  `reqSeats` smallint(6) NOT NULL,
  `totalPrice` int(11) NOT NULL,
  `creationDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `incomeChannel` enum('WEB','MOB') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`userId`,`routeId`,`tripDateTime`,`firstStopId`,`lastStopId`),
  KEY `reservation_availability_from` (`routeId`,`tripDateTime`,`firstStopId`),
  KEY `reservation_availability_to` (`routeId`,`tripDateTime`,`lastStopId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Routes`;
CREATE TABLE IF NOT EXISTS `Routes` (
  `routeId` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `baseRate` int(11) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`routeId`),
  KEY `company_route` (`companyId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Stops`;
CREATE TABLE IF NOT EXISTS `Stops` (
  `stopId` int(11) NOT NULL,
  `routeId` int(11) NOT NULL,
  `stopName` varchar(60) CHARACTER SET utf8 NOT NULL,
  `stopAddress` varchar(80) COLLATE utf8_spanish_ci NOT NULL,
  `latitude` float(10,6) NOT NULL,
  `longitude` float(10,6) NOT NULL,
  `stopElapsedTime` int(11) NOT NULL,
  `stopAdditionalFee` int(11) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`stopId`,`routeId`),
  KEY `route_stop` (`routeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Tickets`;
CREATE TABLE IF NOT EXISTS `Tickets` (
  `userId` bigint(20) NOT NULL,
  `routeId` int(11) NOT NULL,
  `tripDateTime` datetime NOT NULL,
  `firstStopId` int(11) NOT NULL,
  `lastStopId` int(11) NOT NULL,
  `incomeChannel` enum('WEB','MOB') COLLATE utf8_spanish_ci NOT NULL,
  `ticketDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `numberOfSeats` smallint(6) NOT NULL,
  `ticketTotalPrice` int(11) NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`userId`,`routeId`,`tripDateTime`),
  KEY `user_ticket` (`userId`),
  KEY `firstStop_ticket` (`firstStopId`,`routeId`),
  KEY `lastStop_ticket` (`lastStopId`,`routeId`),
  KEY `route_ticket` (`routeId`),
  KEY `ticket_trip` (`routeId`,`tripDateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Trips`;
CREATE TABLE IF NOT EXISTS `Trips` (
  `routeId` int(11) NOT NULL,
  `tripDateTime` datetime NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`routeId`,`tripDateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

DROP TABLE IF EXISTS `Users`;
CREATE TABLE IF NOT EXISTS `Users` (
  `userId` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `customerId` int(11) NOT NULL,
  `userUpDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `enabled` bit(1) NOT NULL,
  PRIMARY KEY (`userId`),
  KEY `customer_user` (`customerId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


ALTER TABLE `Availability`
  ADD CONSTRAINT `stop_availability` FOREIGN KEY (`stopId`,`routeId`) REFERENCES `Stops` (`stopId`, `routeId`),
  ADD CONSTRAINT `trip_availability` FOREIGN KEY (`routeId`,`tripDateTime`) REFERENCES `Trips` (`routeId`, `tripDateTime`);

ALTER TABLE `CompanyUsers`
  ADD CONSTRAINT `companyUser_customer` FOREIGN KEY (`customerDni`) REFERENCES `Customers` (`customerId`),
  ADD CONSTRAINT `customer_company` FOREIGN KEY (`companyId`) REFERENCES `Companies` (`companyId`);

ALTER TABLE `HistoricalUserTickets`
  ADD CONSTRAINT `history_route` FOREIGN KEY (`routeId`) REFERENCES `Routes` (`routeId`),
  ADD CONSTRAINT `history_ticket` FOREIGN KEY (`userId`,`routeId`,`tripDateTime`) REFERENCES `Tickets` (`userId`, `routeId`, `tripDateTime`),
  ADD CONSTRAINT `history_user` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`);

ALTER TABLE `PendingReservations`
  ADD CONSTRAINT `reservation_availability_from` FOREIGN KEY (`routeId`,`tripDateTime`,`firstStopId`) REFERENCES `Availability` (`routeId`, `tripDateTime`, `stopId`),
  ADD CONSTRAINT `reservation_availability_to` FOREIGN KEY (`routeId`,`tripDateTime`,`lastStopId`) REFERENCES `Availability` (`routeId`, `tripDateTime`, `stopId`),
  ADD CONSTRAINT `reservation_route` FOREIGN KEY (`routeId`) REFERENCES `Routes` (`routeId`),
  ADD CONSTRAINT `reservation_user` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`);

ALTER TABLE `Routes`
  ADD CONSTRAINT `company_route` FOREIGN KEY (`companyId`) REFERENCES `Companies` (`companyId`);

ALTER TABLE `Stops`
  ADD CONSTRAINT `route_stop` FOREIGN KEY (`routeId`) REFERENCES `Routes` (`routeId`);

ALTER TABLE `Tickets`
  ADD CONSTRAINT `firstStop_ticket` FOREIGN KEY (`firstStopId`,`routeId`) REFERENCES `Stops` (`stopId`, `routeId`),
  ADD CONSTRAINT `lastStop_ticket` FOREIGN KEY (`lastStopId`,`routeId`) REFERENCES `Stops` (`stopId`, `routeId`),
  ADD CONSTRAINT `route_ticket` FOREIGN KEY (`routeId`) REFERENCES `Routes` (`routeId`),
  ADD CONSTRAINT `ticket_trip` FOREIGN KEY (`routeId`,`tripDateTime`) REFERENCES `Trips` (`routeId`, `tripDateTime`),
  ADD CONSTRAINT `ticket_user` FOREIGN KEY (`userId`) REFERENCES `Users` (`userId`);

ALTER TABLE `Trips`
  ADD CONSTRAINT `trip_route` FOREIGN KEY (`routeId`) REFERENCES `Routes` (`routeId`);

ALTER TABLE `Users`
  ADD CONSTRAINT `customer_user` FOREIGN KEY (`customerId`) REFERENCES `Customers` (`customerId`);
COMMIT;

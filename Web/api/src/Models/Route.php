<?php

namespace App\Models;

class Route
{

	private $routeId;
	private $companyId;
	private $name;
	private $baseRate;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setId ($value) {

    	$this->routeId = $value;
    }

    public function setCompanyId ($value) {

    	$this->companyId = $value;
    }

    public function setName ($value) {

    	$this->name = $value;
    }

    public function setBaseRate ($value) {

    	$this->baseRate = $value;
    }

    public function getId () {

    	return $this->routeId;
    }

    public function getCompanyId () {

    	return $this->companyId;
    }

    public function getName () {

    	return $this->name;
    }

    public function getBaseRate () {

    	return $this->baseRate;
    }
}
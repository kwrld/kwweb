<?php

Namespace App\Models;

class Customer
{
    private $username;
	private $customerId;
	private $firstName;
	private $lastName;
	private $birthdate;
	private $gender;
	private $telephone;
	private $email;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setUsername ($value) {

        $this->username = $value;
    }

    public function setCustomerId ($value) {

    	$this->customerId = $value;
    }

    public function setFirstName ($value) {

    	$this->firstName = $value;
    }

    public function setLastName ($value) {

    	$this->lastName = $value;
    }

    public function setBirthdate ($value) {

    	$this->birthdate = $value;
    }

    public function setGender ($value) {

    	$this->gender = $value;
    }

    public function setTelephone ($value) {

    	$this->telephone = $value;
    }

    public function setEmail ($value) {

    	$this->email = $value;
    }

    public function getUsername () {

        return $this->username;
    }

    public function getCustomerId () {

    	return $this->customerId;
    }

    public function getFirstName () {

    	return $this->firstName;
    }

    public function getLastName () {

    	return $this->lastName;
    }

    public function getBirthdate () {

    	return $this->birthdate;
    }

    public function getGender () {

    	return $this->gender;
    }

    public function getTelephone () {

    	return $this->telephone;
    }

    public function getEmail () {

    	return $this->email;
    }
}
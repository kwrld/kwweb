<?php

namespace App\Models;

class Company {

    private $companyId;
    private $companyCuit;
    private $companyLegalName;
    private $companyFantasyName;
    private $companyUpDate;
    private $telephone;
    private $mail;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setCompanyId ($value) {

    	$this->companyId = $value;
    }

    public function setCompanyCuit ($value) {

    	$this->companyCuit = $value;
    }

    public function setLegalName ($value) {

    	$this->companyLegalName = $value;
    }

    public function setFantasyName ($value) {

    	$this->companyFantasyName = $value;
    }

    public function setCompanyUpDate ($value) {

    	$this->companyUpDate = $value;
    }

    public function setTelephone ($value) {

    	$this->telephone = $value;
    }

    public function setMail ($value) {

    	$this->mail = $value;
    }

    public function getCompanyId () {

    	return $this->companyId;
    }

    public function getCompanyCuit () {

    	return $this->companyCuit;
    }

    public function getLegalName () {

    	return $this->companyLegalName;
    }

    public function getFantasyName () {

    	return $this->companyFantasyName;
    }

    public function getCompanyUpDate () {

    	return $this->companyUpDate;
    }

    public function getTelephone () {

    	return $this->telephone;
    }

    public function getMail () {

    	return $this->mail;
    }
}
<?php

namespace App\Models;

class Trip {

	private $routeId;
	private $tripDateTime;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setRouteId ($value) {

    	$this->routeId = $value;
    }

    public function setTripDateTime ($value) {

        $this->tripDateTime = $value;
    }

    public function setAvailability ($value) {

        $this->availability = $value;
    }

    public function getRouteId () {

    	return $this->routeId;
    }

    public function getTripDateTime () {

        return $this->tripDateTime;
    }
}
<?php

namespace App\Models;

class Ticket
{

	private $userId;
    private $routeId;
    private $tripDateTime;
	private $firstStopId;
	private $lastStopId;
    private $incomeChannel;
    private $ticketDateTime;
    private $numberOfSeats;
    private $ticketTotalPrice;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setUserId ($value) {

    	$this->userId = $value;
    }

    public function setRouteId ($value) {

    	$this->routeId = $value;
    }

    public function setTripDateTime ($value) {

    	$this->tripDateTime = $value;
    }

    public function setFirstStopId ($value) {

    	$this->firstStopId = $value;
    }

    public function setLastStopId ($value) {

    	$this->lastStopId = $value;
    }

    public function setIncomeChannel ($value) {

        $this->incomeChannel = $value;
    }

    public function setTicketDateTime ($value) {

        $this->ticketDateTime = $value;
    }

    public function setNumberOfSeats ($value) {

        $this->numberOfSeats = $value;
    }

    public function setTicketTotalPrice ($value) {

        $this->ticketTotalPrice = $value;
    }

    public function getUserId () {

        return $this->userId;
    }

    public function getRouteId () {

        return $this->routeId;
    }

    public function getTripDateTime () {

        return $this->tripDateTime;
    }

    public function getFirstStopId () {

        return $this->firstStopId;
    }

    public function getLastStopId () {

        return $this->lastStopId;
    }

    public function getIncomeChannel () {

        return $this->incomeChannel;
    }

    public function getTicketDateTime () {

        return $this->ticketDateTime;
    }

    public function getNumberOfSeats () {

        return $this->numberOfSeats;
    }

    public function getTicketTotalPrice () {

        return $this->ticketTotalPrice;
    }
}
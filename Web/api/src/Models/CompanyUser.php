<?php

namespace App\Models;

class CompanyUser
{

    private $userId;
    private $companyId;
    private $username;
    private $customerDni;
    private $userUpDate;
    private $password;


	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setUserId ($value) {

    	$this->userId = $value;
    }

    public function setCompanyId ($value) {

        $this->companyId = $value;
    }

    public function setUsername ($value) {

        $this->username = $value;
    }

    public function setCustomerDni ($value) {

    	$this->customerDni = $value;
    }

    public function setUserUpDate ($value) {

    	$this->userUpDate = $value;
    }

    public function setPassword ($value) {

        $this->password = $value;
    }

    public function getUserId () {

    	return $this->userId;
    }

    public function getCompanyId () {

    	return $this->companyId;
    }

    public function getUsername () {

        return $this->username;
    }

    public function getCustomerDni () {

        return $this->customerDni;
    }

    public function getUserUpDate () {

    	return $this->userUpDate;
    }

    public function getPassword () {

        return $this->password;
    }
}
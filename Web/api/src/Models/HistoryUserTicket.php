<?php

namespace App\Models;

class HistoryUserTicket
{

	private $userId;
    private $routeId;
    private $tripDateTime;
    private $numberOfSeats;
    private $ticketTotalPrice;
    private $ticketDateTime;
    private $incomeChannel;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setUserId ($value) {

    	$this->userId = $value;
    }

    public function setRouteId ($value) {

    	$this->routeId = $value;
    }

    public function setTripDateTime ($value) {

    	$this->tripDateTime = $value;
    }

    public function setIncomeChannel ($value) {

        $this->incomeChannel = $value;
    }

    public function setTicketDateTime ($value) {

        $this->ticketDateTime = $value;
    }

    public function setNumberOfSeats ($value) {

        $this->numberOfSeats = $value;
    }

    public function setTicketTotalPrice ($value) {

        $this->ticketTotalPrice = $value;
    }

    public function getUserId () {

        return $this->userId;
    }

    public function getRouteId () {

        return $this->routeId;
    }

    public function getTripDateTime () {

        return $this->tripDateTime;
    }

    public function getIncomeChannel () {

        return $this->incomeChannel;
    }

    public function getTicketDateTime () {

        return $this->ticketDateTime;
    }

    public function getNumberOfSeats () {

        return $this->numberOfSeats;
    }

    public function getTicketTotalPrice () {

        return $this->ticketTotalPrice;
    }
}
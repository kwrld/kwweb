<?php

Namespace App\Models;

class Stop
{

	private $stopId;
	private $routeId;
	private $stopName;
	private $stopAddress;
	private $latitude;
	private $longitude;
	private $stopElapsedTime;
	private $stopAdditionalFee;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setStopId ($value) {

    	$this->stopId = $value;
    }

    public function setRouteId ($value) {

    	$this->routeId = $value;
    }

    public function setStopName ($value) {

    	$this->stopName = $value;
    }

    public function setStopAddress ($value) {

    	$this->stopAddress = $value;
    }

    public function setLatitude ($value) {

    	$this->latitude = $value;
    }

    public function setLongitude ($value) {

    	$this->longitude = $value;
    }

    public function setStopElapsedTime ($value) {

    	$this->stopElapsedTime = $value;
    }

    public function setStopAdditionalFee ($value) {

    	$this->stopAdditionalFee = $value;
    }

    public function getStopId () {

    	return $this->stopId;
    }

    public function getRouteId () {

    	return $this->routeId;
    }

    public function getStopName () {

    	return $this->stopName;
    }

    public function getStopAddress () {

    	return $this->stopAddress;
    }

    public function getLatitude () {

    	return $this->latitude;
    }

    public function getLongitude () {

    	return $this->longitude;
    }

    public function getStopElapsedTime () {

    	return $this->stopElapsedTime;
    }

    public function getStopAdditionalFee () {

    	return $this->stopAdditionalFee;
    }
}
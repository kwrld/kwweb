<?php

namespace App\Models;

class PendingReservation
{

	private $userId;
    private $routeId;
    private $tripDateTime;
	private $firstStopId;
    private $lastStopId;
    private $reqSeats;
    private $totalPrice;
    private $creationDateTime;
    private $incomeChannel;

	function __construct ($data = array()) {

        foreach ($data as $key => $value) {

            $this->$key = $value;
        }
    }

    public function setUserId ($value) {

    	$this->userId = $value;
    }

    public function setRouteId ($value) {

    	$this->routeId = $value;
    }

    public function setTripDateTime ($value) {

    	$this->tripDateTime = $value;
    }

    public function setFirstStopId ($value) {

    	$this->firstStopId = $value;
    }

    public function setLastStopId ($value) {

    	$this->lastStopId = $value;
    }

    public function setIncomeChannel ($value) {

        $this->incomeChannel = $value;
    }

    public function setCreationDateTime ($value) {

        $this->creationDateTime = $value;
    }

    public function setReqSeats ($value) {

        $this->reqSeats = $value;
    }

    public function setTotalPrice ($value) {

        $this->totalPrice = $value;
    }

    public function getUserId () {

        return $this->userId;
    }

    public function getRouteId () {

        return $this->routeId;
    }

    public function getTripDateTime () {

        return $this->tripDateTime;
    }

    public function getFirstStopId () {

        return $this->firstStopId;
    }

    public function getLastStopId () {

        return $this->lastStopId;
    }

    public function getIncomeChannel () {

        return $this->incomeChannel;
    }

    public function getCreationDateTime () {

        return $this->creationDateTime;
    }

    public function getReqSeats () {

        return $this->reqSeats;
    }

    public function getTotalPrice () {

        return $this->totalPrice;
    }
}
<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['KWDB'] = function ($c) {
    $db = $c['settings']['KWDB'];
    $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'], $db['user'], $db['password']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    return $pdo;
};

$container['repository'] = function($c) {
    return new App\DataAccess\DataLayer($c->KWDB);
};

$container['CompaniesController'] = function ($c)
{
    return new App\Controllers\CompaniesController($c->repository, $c->token);
};

$container['CompaniesUsersController'] = function ($c)
{
    return new App\Controllers\CompaniesUsersController($c->repository, $c->token);
};

$container['CustomersController'] = function ($c)
{
    return new App\Controllers\CustomersController($c->repository, $c->token);
};

$container['PersonsController'] = function ($c)
{
    return new App\Controllers\PersonsController($c->repository, $c->token);
};

$container['RoutesController'] = function ($c)
{
    return new App\Controllers\RoutesController($c->repository, $c->token);
};

$container['StopsController'] = function ($c)
{
    return new App\Controllers\StopsController($c->repository, $c->token);
};

$container['TicketsController'] = function ($c)
{
    return new App\Controllers\TicketsController($c->repository, $c->token);
};

$container['PendingReservationsController'] = function ($c)
{
    return new App\Controllers\PendingReservationsController($c->repository, $c->token);
};

$container['HistoricalUserTicketsController'] = function ($c)
{
    return new App\Controllers\HistoricalUserTicketsController($c->repository, $c->token);
};

$container['TripsController'] = function ($c)
{
    return new App\Controllers\TripsController($c->repository, $c->token);
};

$container['UsersController'] = function ($c)
{
    return new \App\Controllers\UsersController($c->repository, $c->token);
};
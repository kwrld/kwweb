<?php

namespace App\Models;
namespace App\DataAccess;

use App\Models\Company;
use App\Models\CompanyUser;
use App\Models\Customer;
use App\Models\HistoryUserTicket;
use App\Models\PendingReservation;
use App\Models\Route;
use App\Models\Stop;
use App\Models\Ticket;
use App\Models\Trip;
use App\Models\User;

class DataLayer
{

	protected $KWDB;

	public function __construct ($pdo)
	{
		$this->KWDB = $pdo;
	}

	/*
		COMPANIES
	*/

	public function getAllCompanies () {

		try {

			$query = "CALL get_companies ()";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyById ($companyId) {

		try {

			$query = "CALL get_company ('$companyId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyByName ($companyName) {

		try {

			$query = "CALL get_companies_by_name ('$companyName');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createCompany (Company $aCompany) {

		try {

			$cuit = $aCompany->getCuit();
			$name = $aCompany->getLegalName();
			$commercialName = $aCompany->getFantasyName();
			$tel = $aCompany->getTelephone();
			$mail = $aCompany->getMail();

			$query = "CALL add_company ('$cuit', '$name', '$commercialName', '$tel', '$mail');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function deleteCompany ($companyId) {

		try {

			$query = "CALL del_company ('$companyId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateCompany (Company $aCompany) {

		try {

			$companyId = $aCompany->getId();
			$cuit = $aCompany->getCuit();
			$name = $aCompany->getLegalName();
			$commercialName = $aCompany->getFantasyName();
			$tel = $aCompany->getTelephone();
			$mail = $aCompany->getMail();

			$query = "CALL upd_company ('$companyId', '$cuit', '$name', '$commercialName', '$tel', '$mail');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	/*
		USERS
	*/

	public function login (User $aUser) {

		try
		{
			$username = $aUser->getUsername();
			$pass = $aUser->getPassword();

			$query = "CALL login ('$username', '$pass')";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;
	}

	public function getUserById ($userID) {

		try {

			$query = "CALL get_user_by_id ('$userID');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getUserByName ($username) {

		try {

			$query = "CALL get_user ('$username');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createUser (User $aUser) {

		try
		{
			$dni = (int) $aUser->getCustomerDni();
			$username = $aUser->getUsername();
			$password = $aUser->getPassword();

			$query = "CALL add_user ('$dni', '$username', '$password');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function deleteUser ($username) {

		try
		{
			$query = "CALL del_user ('$username');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function updateUser (User $aUser) {

		try
		{

			$username = $aUser->getUsername();
			$password = $aUser->getPassword();

			$query = "CALL upd_user_password ('$username', '$password');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function passwordReset ($username, $dni) {

		try
		{

			$query = "CALL user_password_reset ('$username', '$dni');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}


	/*
		COMPANYUSERS
	*/

	public function loginCompanyUser (CompanyUser $aUser) {

		try
		{
			$username = $aUser->getUsername();
			$pass = $aUser->getPassword();

			$query = "CALL login_company_user ('$username', '$pass')";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;
	}

	public function getCompanyUserById ($userID) {

		try {

			$query = "CALL get_company_user_by_id ('$userID');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyUserByName ($username) {

		try {

			$query = "CALL get_company_user_by_username ('$username');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createCompanyUser (CompanyUser $aUser) {

		try
		{
			$dni = (int) $aUser->getCustomerDni();
			$companyId = (int) $aUser->getCompanyId();
			$username = $aUser->getUsername();
			$password = $aUser->getPassword();

			$query = "CALL add_company_user ('$username', '$dni', '$companyId', '$password');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function deleteCompanyUser ($username) {

		try
		{
			$query = "CALL del_company_user ('$username');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function updateCompanyUser (CompanyUser $aUser) {

		try
		{

			$username = $aUser->getUsername();
			$password = $aUser->getPassword();

			$query = "CALL upd_company_user ('$username', '$password');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function companyUsersPasswordReset ($username, $dni) {

		try
		{

			$query = "CALL company_user_password_reset ('$username', '$dni');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}


	/*
		PERSONS
	*/

	public function createCustomer (Customer $aCustomer) {

		try
		{

			$dni = (int) $aCustomer->getCustomerDni();
			$name = $aCustomer->getFirstName();
			$lastName = $aCustomer->getLastName();
			$birthdate = $aCustomer->getBirthdate();
			$gender = $aCustomer->getGender();
			$telephone = $aCustomer->getTelephone();
			$mail = $aCustomer->getMail();

			$query = "CALL add_customer ('$dni', '$name', '$lastName', '$birthdate', '$gender', '$telephone', '$mail');";

			$result = $this->KWDB->query($query);

		}
		catch (Exception $ex)
		{
			$result = false;
		}

		return $result;

	}

	public function deleteCustomer ($dni) {

		try {

			$query = "CALL del_customer ('$dni');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateCustomerWeb (Customer $aCustomer) {

		try {

			$dni = (int) $aCustomer->getCustomerDni();
			$name = $aCustomer->getFirstName();
			$lastName = $aCustomer->getLastName();
			$birthdate = $aCustomer->getBirthdate();
			$gender = $aCustomer->getGender();
			$telephone = $aCustomer->getTelephone();
			$mail = $aCustomer->getMail();

			$query = "CALL upd_customer_from_web ('$dni', '$name', '$lastName', '$birthdate', '$gender', '$telephone', '$mail');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateCustomerMobile (Customer $aCustomer) {

		try {
			$username = $aCustomer->getUsername();
			$name = $aCustomer->getFirstName();
			$lastName = $aCustomer->getLastName();
			$birthdate = $aCustomer->getBirthdate();
			$gender = $aCustomer->getGender();
			$telephone = $aCustomer->getTelephone();
			$mail = $aCustomer->getEmail();

			$query = "CALL upd_customer_from_mobile ('$username', '$name', '$lastName', '$birthdate', '$gender', '$telephone', '$mail');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function getCustomerByUsername ($username) {

		try {

			$query = "CALL get_customer_by_username ('$username');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}


	/*
		ROUTES
	*/

	public function getRoutesFromCompany ($companyId) {

		try {

			$query = "CALL get_routes ('$companyId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getRoute ($companyId, $routeId) {

		try {

			$query = "CALL get_route ('$companyId', '$routeId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createRoute (Route $aRoute) {

		try {

			$companyId = (int) $aRoute->getCompanyId();
			$name = $aRoute->getName();
			$BaseRate = (int) $aRoute->getBaseRate();

			$query = "CALL add_route ('$companyId', '$name', '$BaseRate');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function deleteRoute ($companyId, $routeId) {

		try {

			$query = "CALL del_route ('$routeId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateRoute (Route $aRoute) {

		try {

			$companyId = (int) $aRoute->getCompanyId();
			$routeId = (int) $aRoute->getId();
			$name = $aRoute->getName();
			$BaseRate = (int) $aRoute->getBaseRate();

			$query = "CALL upd_route ('$companyId', '$routeId', '$name', '$BaseRate');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}


	/*
		STOPS
	*/

	public function getStopsFromRoute ($routeId) {

		try {

			$query = "CALL get_stops_by_route ('$routeId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getNextStops ($routeId, $stopId) {

		try {

			$query = "CALL 	get_stops_from_another_stop ('$routeId', '$stopId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getNearbyStops ($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $radius) {

		try {

			$query = "CALL get_nearby_stops ('$latitudeFrom', '$longitudeFrom', '$latitudeTo', '$longitudeTo', '$radius');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createStop (Stop $aStop) {

		try {

			$routeId = (int) $aStop->getRouteId();
			$name = $aStop->getStopName();
			$address = $aStop->getStopAddress();
			$latitude = $aStop->getLatitude();
			$longitude = $aStop->getLongitude();
			$elapsedTime = (int) $aStop->getStopElapsedTime();
			$additionalFee = (int) $aStop->getStopAdditionalFee();

			$query = "CALL add_stop ('$routeId', '$name', '$address', '$latitude', '$longitude', '$elapsedTime', '$additionalFee');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function deleteStop ($routeId, $stopId) {

		try {

			$query = "CALL del_stop ('$routeId', '$stopId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateStop (Stop $aStop) {

		try {

			$stopId = (int) $aStop->getStopId();
			$routeId = (int) $aStop->getRouteId();
			$name = $aStop->getStopName();
			$address = $aStop->getStopAddress();
			$latitude = $aStop->getLatitude();
			$longitude = $aStop->getLongitude();
			$elapsedTime = $aStop->getStopElapsedTime();
			$additionalFee = $aStop->getStopAdditionalFee();

			$query = "CALL upd_stop ('$routeId', '$stopId', '$name', '$address', '$latitude', '$longitude',
										'$elapsedTime', '$additionalFee');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}


	/*
		TRIPS
	*/


	public function getRouteTripsByDateTime ($companyId, $routeId, $dateTime) {

		/*
			Viajes - WEB - Obtener todos los trips de una fecha a partir de una fecha y hora
		*/

		try {

			$query = "CALL get_trips_route_from_datetime ('$companyId', '$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getStopsAvailabilityByDate ($routeId, $date, $qty, $fromId, $toId) {

		/*
			Viajes - BOTH - Obtener la disponibilidad y horarios por parada de todos los viajes
							de un recorrido de una compañía por fecha.
		*/

		try {

			$query = "CALL get_available_trips_by_date ('$routeId', '$date', '$fromId', '$toId', '$qty');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyTripsByRouteAndDate ($routeId, $date, $qty, $fromId, $toId) {

		/*
			Viajes - BOTH - Obtener todos los viajes de un recorrido de una compañía por fecha y disponibilidad
		*/

		try {

			$query = "CALL get_company_trips_route_and_date	('$routeId', '$date', '$fromId', '$toId', '$qty');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getRouteTripsBetweenDates ($companyId, $routeId, $fromDateTime, $toDateTime) {


		/*
			Viajes - WEB - Obtener todos los trips del route de una company entre dos fechas
		*/

		try {

			$query = "CALL get_trips_between_dates ('$companyId', '$routeId', '$fromDateTime', '$toDateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function getRouteTripsByDate ($routeId, $date) {

		/*
			Viajes - WEB - Obtener todos los trips de una company by fecha y route
		*/

		try {

			$query = "CALL get_route_trips_by_date ('$routeId', '$date');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function getAllTripsFromDateTime ($dateTime) {

		/*
			VIAJES - WEB - Obtener todos los trips de todas las compañías partir de una fecha y hora
		*/

		try {

			$query = "CALL get_all_trips_from_datetime ('$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function getAllTripsByDate ($date) {

		/*
			Viajes - WEB - Obtener todos los trips de una fecha
		*/

		try {

			$query = "CALL get_all_trips_in_date ('$date');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyTripsByDate ($companyId, $routeId, $date) {

		/*
			Viajes - WEB - Obtener todos los trips de una company by fecha
		*/

		try {

			$query = "CALL get_route ('$companyId', '$routeId', '$date');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function massiveTripsCreation ($aSchedule) {

		/*
			Viajes - WEB - Generar todos los trips de un route de una company en forma masiva y automática a partir de una fecha, de una hora desde-hasta y de un intervalo de tiempo
		*/

		try {

			$fromDateTime = $aSchedule['fromDateTime'];
			$toDateTime = $aSchedule['toDateTime'];
			$frequency = (int) $aSchedule['frequency'];
			$onMondays = (bool) $aSchedule['onMondays'];
			$onTuedays = (bool) $aSchedule['onTuedays'];
			$onWednesdays = (bool) $aSchedule['onWednesdays'];
			$onThursdays = (bool) $aSchedule['onThursdays'];
			$onFridays = (bool) $aSchedule['onFridays'];
			$onSaturdays = (bool) $aSchedule['onSaturdays'];
			$onSundays = (bool) $aSchedule['onSundays'];
			$routeId = (int) $aSchedule['routeId'];

			$query = "CALL massive_trip_generation ('$fromDateTime', '$toDateTime', '$frequency', '$onMondays', '$onTuedays',
													'$onWednesdays', '$onThursdays', '$onFridays', '$onSaturdays', '$onSundays',
													'$routeId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function createTrip (Trip $aTrip) {

		/*
			Viajes - WEB - Alta
		*/

		try {

			$routeId = (int) $aTrip->getRouteId();
			$dateTime = $aTrip->getTripDateTime();

			$query = "CALL add_trip ('$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateTrip (Trip $aTrip) {

		try {

			$routeId = $aTrip->getRouteId();
			$dateTime = $aTrip->getTripDateTime();

			$query = "CALL upd_trip ('$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function deleteTrip ($routeId, $dateTime) {

		/*
			Viajes - WEB - Baja
		*/

		try {

			$query = "CALL del_trip ('$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}


	/*
		TICKETS
	*/


	public function getUserTickets ($userId, $dateTime, $qty) {

		/*
			Tickets - BOTH - Obtener tickets del user, paginados
		*/

		try {

			$query = "CALL get_user_tickets ('$userId', '$dateTime', '$qty');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getTicketsByTrip ($companyId, $routeId, $dateTime) {

		/*
			Tickets - BOTH - Obtener tickets de un trip
		*/

		try {

			$query = "CALL get_trip_tickets ('$companyId', '$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getTicketsByRouteAndDate ($companyId, $routeId, $date) {

		/*
			Tickets - BOTH - Obtener tickets de un route by fecha
		*/

		try {

			$query = "CALL get_tickets_by_route_and_date ('$companyId', '$routeId', '$date');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function getCompanyTicketsByDate ($companyId, $date) {

		/*
			Tickets - BOTH - Obtener tickets de una fecha by company
		*/

		try {

			$query = "CALL get_company_tickets_by_date ('$companyId', '$date');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;
	}

	public function createTicket (Ticket $aTicket) {

		try {

			$userId = (int) $aTicket->getUserId();
			$routeId = (int) $aTicket->getRouteId();
			$tripDateTime = $aTicket->getTripDateTime();
			$initialStopId = (int) $aTicket->getFirstStopId();
			$finalStopId = (int) $aTicket->getLastStopId();
			$channel = $aTicket->getIncomeChannel();
			$qty = (int) $aTicket->getNumberOfSeats();
			$price = (int) $aTicket->getTicketTotalPrice();

			$query = "CALL add_ticket ('$userId', '$routeId', '$tripDateTime', '$initialStopId',
										'$finalStopId', '$channel', '$qty', '$price');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function deleteTicket ($userId, $companyId, $routeId, $dateTime) {

		try {

			$query = "CALL del_ticket ('$userId', '$companyId', '$routeId', '$dateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function updateTicket (Ticket $aTicket) {

		try {

			$userId = (int) $aTicket->getUserId();
			$routeId = (int) $aTicket->getRouteId();
			$tripDateTime = $aTicket->getTripDateTime();
			$initialStopId = (int) $aTicket->getFirstStopId();
			$finalStopId = (int) $aTicket->getLastStopId();
			$channel = $aTicket->getIncomeChannel();
			$qty = (int) $aTicket->getNumberOfSeats();
			$price = (int) $aTicket->getTicketTotalPrice();

			$query = "CALL upd_ticket ('$userId', '$routeId', '$tripDateTime', '$initialStopId',
										'$finalStopId', '$channel', '$qty', '$price');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}


	/*
		PENDING RESERVATIONS
	*/

	public function createPendingReservation (PendingReservation $aReservation) {

		try {

			$userId = (int) $aReservation->getUserId();
			$routeId = (int) $aReservation->getRouteId();
			$tripDateTime = $aReservation->getTripDateTime();
			$firstStopId = (int) $aReservation->getFirstStopId();
			$lastStopId = (int) $aReservation->getLastStopId();
			$reqSeats = (int) $aReservation->getReqSeats();
			$incomeChannel = $aReservation->getIncomeChannel();

			$query = "CALL set_pre_reservation ('$routeId', '$tripDateTime', '$userId', '$reqSeats', '$firstStopId',
												'$lastStopId', '$incomeChannel');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function confirmPendingReservation ($userId, $routeId, $tripDateTime) {

		try {

			$query = "CALL reservation_confirmation ('$userId', '$routeId', '$tripDateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	public function cancelPendingReservation ($userId, $routeId, $tripDateTime) {

		try {

			$query = "CALL del_pending_reservation ('$userId', '$routeId', '$tripDateTime');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}

	/*
		HISTORY USER TICKETS
	*/

	public function getHistoricalUserTickets ($userId) {

		try {

			$query = "CALL get_historical_user_tickets ('$userId');";

			$result = $this->KWDB->query($query);

		} catch (Exception $e) {

			$result = false;

		}

		return $result;

	}
}
<?php
// Application middleware

// e.g: $app->add(new \Slim\Csrf\Guard);

use App\Models\Token;
use Tuupola\Middleware\JwtAuthentication;
use App\Middleware;

$container = $app->getContainer();

$container["token"] = function ($container) {
    return new Token;
};

$container["JwtAuthentication"] = function ($container) {
    return new JwtAuthentication([
        "path" => ["/v1/web"],
        "passthrough" => ["/v1/auth"],
        "secret" => "corsita2008",
        "logger" => $container["logger"],
        "callback" => function ($request, $response, $arguments) use ($container) {
            $container["token"]->hydrate($arguments["decoded"]);
        }
    ]);
};

$container["JSONMiddleware"] = function ($container) {
    return new JSONMiddleware('/v1/');
};

$app->add("JwtAuthentication");
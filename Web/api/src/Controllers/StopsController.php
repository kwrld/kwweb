<?php

namespace App\Controllers;

use App\Models\Stop;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class StopsController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function getAllStops ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$stops = $this->repo->getStopsFromRoute($routeId);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getNextStops ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$stopId = (int) $args['stopID'];
			$stops = $this->repo->getNextStops($routeId, $stopId);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getNearbyStops ($request, $response, $args) {

		try {

			$latitudeFrom = (float) $args['latitudeFrom'];
			$longitudeFrom = (float) $args['longitudeFrom'];
			$latitudeTo = (float) $args['latitudeTo'];
			$longitudeTo = (float) $args['longitudeTo'];
			$radius = (int) $args['radius'];
			$stops = $this->repo->getNearbyStops($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $radius);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function create ($request, $response, $args) {

		try {

			$aStop = new Stop($request->getParsedBody()['stop']);
			$stops = $this->repo->createStop($aStop);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$stopId = (int) $args['stopID'];
			$stops = $this->repo->deleteStop($routeId, $stopId);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aStop = new Stop($request->getParsedBody()['stop']);
			$stops = $this->repo->updateStop($aStop);
			$result['stops'] = $stops->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
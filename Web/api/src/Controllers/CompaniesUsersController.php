<?php

namespace App\Controllers;

use App\Models\CompanyUser;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class CompanyUsersController
{

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function login ($request, $response)
	{
		$time = time();
		$key = 'corsita2008';

		$user = new CompanyUser($request->getParam('user'));

		try {

			$result = $this->repo->loginCompanyUser($user);
			$row = $result->fetch();
			$result->closeCursor();

			if ($row['OK'] == 1)
			{
				$user->setID($row['userID']);

				$token = array(
				    'iat' => $time, // Tiempo que inició el token
				    'exp' => $time + (60 * 60), // Tiempo que expirará el token (+1 hora)
				    'username' => $row['username'],
				    'id' => $row['userID']
				);

				$jwt = JWT::encode($token, $key);

				return $response->withHeader('Authorization', 'Bearer '. $jwt)
								->withStatus(200);
			}
			else
			{
				return $response->withStatus(401);
			}

		} catch (Exception $ex) {

			return $response->withStatus(401);

		}
	}

	public function getByID ($request, $response, $args) {

		try {

			$userID = (int) $args['userID'];
			$user = $this->repo->getCompanyUserById($userID);
			$result['user'] = $user->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getByName ($request, $response, $args) {

		try {

			$username = $args['username'];
			$user = $this->repo->getCompanyUserByName($username);
			$result['user'] = $user->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function create ($request, $response, $args) {

		try {

			$aUser = new CompanyUser($request->getParsedBody()['user']);
			$users = $this->repo->createCompanyUser($aUser);
			$result = $users->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$username = $args['username'];
			$users = $this->repo->deleteCompanyUser($username);
			$result = $users->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aUser = new CompanyUser($request->getParsedBody()['user']);
			$users = $this->repo->updateCompanyUser($aUser);
			$result = $users->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function companyUsersPasswordReset ($request, $response, $args) {

		try {

			$username = $args['username'];
			$dni = (int) $args['dni'];
			$newPassword = $this->repo->companyUsersPasswordReset($username, $dni);
			$result['user'] = $newPassword->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
<?php

namespace App\Controllers;

use App\Models\PendingReservation;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class PendingReservationsController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function create ($request, $response, $args) {

		try {

			$aReservation = new PendingReservation($request->getParsedBody()['preReservation']);
			$reservations = $this->repo->createPendingReservation($aReservation);
			$result['reservations'] = $reservations->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function confirm ($request, $response, $args) {

		try {

			$userId = (int) $args['userId'];
			$routeId = (int) $args['routeId'];
			$tripDateTime = $args['tripDateTime'];

			$reservations = $this->repo->confirmPendingReservation($userId, $routeId, $tripDateTime);

			$result = true;

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function cancel ($request, $response, $args) {

		try {

			$userId = (int) $args['userId'];
			$routeId = (int) $args['routeId'];
			$tripDateTime = $args['tripDateTime'];

			$reservations = $this->repo->cancelPendingReservation($userId, $routeId, $tripDateTime);
			$result['reservations'] = $reservations->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
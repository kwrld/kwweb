<?php

namespace App\Controllers;

use App\Models\Ticket;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class TicketsController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function create ($request, $response, $args) {

		try {

			$aTicket = new Ticket($request->getParsedBody()['ticket']);
			$tickets = $this->repo->createTicket($aTicket);

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$userId = (int) $args['userID'];
			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$ticketDateTime = $args['dateTime'];
			$tickets = $this->repo->deleteTicket($userId, $companyId, $routeId, $ticketDateTime);
			$result['tickets'] = $tickets->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aTicket = new Ticket($request->getParsedBody()['ticket']);
			$tickets = $this->repo->updateTicket($aTicket);
			$result['tickets'] = $tickets->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getUserTickets ($request, $response, $args) {

		try {

			$userId = $args['userID'];
			$dateTime = $args['dateTime'];
			$qty = (int) $args['qty'];

			$trips = $this->repo->getUserTickets($userId, $dateTime, $qty);
			$result['tickets'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getTicketsByTrip ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$dateTime = $args['dateTime'];

			$trips = $this->repo->getTicketsByTrip($companyId, $routeId, $dateTime);
			$result['tickets'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getTicketsByRouteAndDate ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$date = $args['date'];

			$trips = $this->repo->getTicketsByRouteAndDate($companyId, $routeId, $date);
			$result['tickets'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getCompanyTicketsByDate ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$date = $args['date'];

			$trips = $this->repo->getCompanyTicketsByDate($companyId, $date);
			$result['tickets'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
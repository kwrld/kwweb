<?php

namespace App\Controllers;

use App\Models\Trip;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class TripsController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function create ($request, $response, $args) {

		try {

			$aTrip = new Trip($request->getParsedBody()['trip']);
			$trips = $this->repo->createTrip($aTrip);
			$result = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$dateTime = $args['dateTime'];

			$trips = $this->repo->deleteTrip($companyId, $routeId, $dateTime);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aTrip = new Trip($request->getParsedBody()['trip']);
			$trips = $this->repo->updateTrip($aTrip);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function massiveTripsCreation ($request, $response, $args) {

		try {

			$aSchedule = $request->getParsedBody()['schedule'];
			$trips = $this->repo->massiveTripsCreation($aSchedule);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getStopsAvailabilityByDate ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$date = $args['date'];
			$qty = (int) $args['qty'];
			$fromId = (int) $args['fromStopID'];
			$toId = (int) $args['lastStopID'];

			$trips = $this->repo->getStopsAvailabilityByDate($routeId, $date, $qty, $fromId, $toId);
			$result['availability'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getCompanyTripsByRouteAndDate ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$date = $args['date'];
			$qty = (int) $args['qty'];
			$fromId = (int) $args['fromStopID'];
			$toId = (int) $args['lastStopID'];
			$trips = $this->repo->getCompanyTripsByRouteAndDate($routeId, $date, $qty, $fromId, $toId);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getAllTripsByDate ($request, $response, $args) {

		try {

			$date = $args['date'];

			$trips = $this->repo->getAllTripsByDate($date);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getCompanyTripsByDate ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$date = $args['date'];

			$trips = $this->repo->getCompanyTripsByDate($companyId, $routeId, $date);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getAllTripsFromDateTime ($request, $response, $args) {

		try {

			$dateTime = $args['dateTime'];
			$trips = $this->repo->getAllTripsFromDateTime($dateTime);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getRouteTripsByDate ($request, $response, $args) {

		try {

			$routeId = (int) $args['routeID'];
			$date = $args['date'];

			$trips = $this->repo->getRouteTripsByDate($routeId, $date);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getRouteTripsBetweenDates ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$fromDateTime = $args['fromDateTime'];
			$toDateTime = $args['toDateTime'];

			$trips = $this->repo->getRouteTripsBetweenDates($companyId, $routeId, $fromDateTime, $toDateTime);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getRouteTripsByDateTime ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$dateTime = $args['dateTime'];

			$trips = $this->repo->getRouteTripsByDateTime($companyId, $routeId, $dateTime);
			$result['trips'] = $trips->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
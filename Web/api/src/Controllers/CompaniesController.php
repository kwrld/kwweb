<?php

namespace App\Controllers;

use App\Models\Company;
use App\DataAccess\DataLayer;

class CompaniesController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function getAll ($request, $response, $args) {

		try {

			$companies = $this->repo->getAllCompanies();
			$result['companies'] = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getByID ($request, $response, $args) {

		try {

			$companyId = (int) $args['id'];
			$companies = $this->repo->getCompanyById($companyId);
			$result['companies'] = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getByName ($request, $response, $args) {

		try {

			$companyName = $args['name'];
			$companies = $this->repo->getCompanyByName($companyName);
			$result['companies'] = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function create ($request, $response, $args) {

		try {

			$aCompany = new Company($request->getParsedBody()['company']);
			$companies = $this->repo->createCompany($aCompany);
			$result = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$companyId = (int) $args['id'];
			$companies = $this->repo->deleteCompany($companyId);
			$result['companies'] = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aCompany = new Company($request->getParsedBody()['company']);
			$companies = $this->repo->updateCompany($aCompany);
			$result['companies'] = $companies->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
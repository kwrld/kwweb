<?php

namespace App\Controllers;

use App\Models\Route;
use App\Models\Company;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class RoutesController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function getRoutesFromCompany ($request, $response, $args) {

		try {

			$companyId = $args['companyID'];
			$routes = $this->repo->getRoutesFromCompany($companyId);
			$result['routes'] = $routes->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getRoute ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$routes = $this->repo->getRoute($companyId, $routeId);
			$result['routes'] = $routes->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function create ($request, $response, $args) {

		try {

			$aRoute = new Route($request->getParsedBody()['route']);
			$routes = $this->repo->createRoute($aRoute);
			$result = $routes->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$companyId = (int) $args['companyID'];
			$routeId = (int) $args['routeID'];
			$routes = $this->repo->deleteRoute($companyId, $routeId);
			$result['routes'] = $routes->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function update ($request, $response, $args) {

		try {

			$aRoute = new Route($request->getParsedBody()['route']);
			$routes = $this->repo->updateRoute($aRoute);
			$result['routes'] = $routes->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
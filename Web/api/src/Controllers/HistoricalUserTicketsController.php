<?php

namespace App\Controllers;

use App\Models\HistoryUserTicket;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class HistoricalUserTicketsController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function getUserHistory ($request, $response, $args) {

		try {

			$userId = (int) $args['userId'];

			$history = $this->repo->getHistoricalUserTickets($userId);

			$result['history'] = $history->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
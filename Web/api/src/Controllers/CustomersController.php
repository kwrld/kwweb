<?php

namespace App\Controllers;

use App\Models\Customer;
use App\DataAccess\DataLayer;
use Slim\Middleware\JwtAuthentication;
use Firebase\JWT\JWT;

class CustomersController {

	protected $repo;
	protected $token;

	public function __construct($repository, $aToken) {

		$this->repo = $repository;
		$this->token = $aToken;

	}

	public function create ($request, $response, $args) {

		try {

			$aCustomer = new Customer($request->getParsedBody()['customer']);
			$customer = $this->repo->createCustomer($aCustomer);
			$result = $customer->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function delete ($request, $response, $args) {

		try {

			$dni = (int) $args['dni'];
			$customer = $this->repo->deleteCustomer($dni);
			$result['customer'] = $customer->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function updateCustomerWeb ($request, $response, $args) {

		try {

			$aCustomer = new Customer($request->getParsedBody()['customer']);
			$customers = $this->repo->updateCustomerWeb($aCustomer);
			$result['customer'] = $customers->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function updateCustomerMobile ($request, $response, $args) {

		try {

			$aCustomer = new Customer($request->getParsedBody()['customer']);
			$customers = $this->repo->updateCustomerMobile($aCustomer);
			$result['customer'] = $customers->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}

	public function getCustomerByUsername ($request, $response, $args) {

		try {
			$username = $args['username'];
			$customers = $this->repo->getCustomerByUsername($username);
			$result['customer'] = $customers->fetchAll();

		} catch (Exception $ex) {

			return $response->withJson($result, 403);

		}

		return $response->withJson($result, 200);

	}
}
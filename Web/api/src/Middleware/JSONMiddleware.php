<?php



class JSONMiddleware
{
    /**
     * Example middleware invokable class
     *
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke($request, $response, $next) {

        $method = strtolower($request->getMethod());
        $mediaType = $request->getMediaType();

        if (in_array($method, array('get', 'post', 'put'))) {
            if (empty($mediaType) || $mediaType !== 'application/json') {
                return $response->withStatus(403);
            }
        }

        $response = $next($request, $response);

        return $response->withHeader('Content-type', 'application/json')
                        ->withHeader('Access-Control-Allow-Origin', 'https://localhost')
                        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT');

    }
}
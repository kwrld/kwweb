<?php
// Routes

/*$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});
*/

use Firebase\JWT\JWT;
use Tuupola\Base62;
use App\Controllers\CompaniesController;
use App\Controllers\CompaniesUsersController;
use App\Controllers\CustomersController;
use App\Controllers\RoutesController;
use App\Controllers\StopsController;
use App\Controllers\TicketsController;
use App\Controllers\TripsController;
use App\Controllers\UsersController;

$app->group('/v1/', function () {
	$this->group('auth/', function() {
		$this->post('', 'UsersController:create');
        $this->post('login', 'UsersController:login');
        $this->post('passwordReset/user/{username}/dni/{dni}', 'UsersController:passwordReset');
	});
	$this->group('both/', function () {
		$this->group('companies/', function () {
	        $this->get('', 'CompaniesController:getAll');
	        $this->get('{id}', 'CompaniesController:getByID');
	        $this->get('name/{name}', 'CompaniesController:getByName');
		});
		$this->group('users/', function () {
	        $this->get('username/{username}', 'UsersController:getByName');
	        $this->get('userID/{ID}', 'UsersController:getByID');
	        $this->put('', 'UsersController:update');
	        $this->put('{username}', 'UsersController:delete');
		});
		$this->group('routes/', function () {
			$this->get('company/{companyID}', 'RoutesController:getRoutesFromCompany');
	        $this->get('route/{routeID}', 'RoutesController:getRoute');
		});
		$this->group('stops/', function () {
			$this->get('route/{routeID}', 'StopsController:getAllStops');
	        $this->get('route/{routeID}/stop/{stopID}', 'StopsController:getNextStops');
	        $this->get('latFrom/{latitudeFrom}/lonFrom/{longitudeFrom}/latTo/{latitudeTo}/lonTo/{longitudeTo}/kms/{radius}', 'StopsController:getNearbyStops');
		});
		$this->group('tickets/', function () {
	        $this->get('user/{userID}/dateTime/{dateTime}/qty/{qty}', 'TicketsController:getUserTickets');
	        $this->post('', 'TicketsController:create');
	        $this->put('', 'TicketsController:update');
	        $this->put('route/{routeID}/user/{userID}/dateTime/{dateTime}', 'TicketsController:delete');
	        $this->get('history/user/{userId}', 'HistoricalUserTicketsController:getUserHistory');
		});
		$this->group('prereservations/', function() {
			$this->post('', 'PendingReservationsController:create');
			$this->post('confirm/user/{userId}/route/{routeId}/trip/{tripDateTime}', 'PendingReservationsController:confirm');
			$this->post('cancel/user/{userId}/route/{routeId}/trip/{tripDateTime}', 'PendingReservationsController:cancel');
		});
		$this->group('customers/', function () {
			$this->get('username/{username}', 'CustomersController:getCustomerByUsername');
	        $this->post('', 'CustomersController:create');
	        $this->put('', 'CustomersController:updateCustomerMobile');
		});
		$this->group('trips/', function () {
	        $this->get('routeId/{routeID}/dateTime/{dateTime}', 'TripsController:getRouteTripsByDateTime');
	        $this->get('routeId/{routeID}/date/{date}', 'TripsController:getRouteTripsByDate');
	        $this->get('routeId/{routeID}/date/{date}/seats/{qty}/from/{fromStopID}/to/{lastStopID}', 'TripsController:getCompanyTripsByRouteAndDate');
	        $this->get('availability/routeId/{routeID}/date/{date}/seats/{qty}/from/{fromStopID}/to/{lastStopID}', 'TripsController:getStopsAvailabilityByDate');
		});
	});
	$this->group('web/', function () {
		$this->group('companies/', function () {
	        $this->post('', 'CompaniesController:create');
	        $this->put('', 'CompaniesController:update');
	        $this->put('company/{id}', 'CompaniesController:delete');
		});
		$this->group('companiesUsers/', function () {
	        $this->get('username/{username}', 'CompanyUsersController:getByName');
	        $this->get('userID/{userID}', 'CompanyUsersController:getByID');
	        $this->put('', 'CompanyUsersController:update');
	        $this->put('{username}', 'CompanyUsersController:delete');
	        $this->post('', 'CompanyUsersController:create');
	        $this->post('login', 'CompanyUsersController:login');
	        $this->post('passwordReset/{username}', 'CompanyUsersController:companyUsersPasswordReset');
	    });
		$this->group('routes/', function () {
	        $this->post('', 'RoutesController:create');
	        $this->put('', 'RoutesController:update');
	        $this->put('{routeID}', 'RoutesController:delete');
		});
		$this->group('stops/', function () {
	        $this->post('', 'StopsController:create');
	        $this->put('', 'StopsController:update');
	        $this->put('route/{routeID}/stop/{stopID}', 'StopsController:delete');
		});
		$this->group('tickets/', function () {
	        $this->get('company/{companyID}/date/{date}', 'TicketsController:getCompanyTicketsByDate');
	        $this->get('company/{companyID}/route/{routeID}/date/{date}', 'TicketsController:getTicketsByRouteAndDate');
	        $this->get('company/{companyID}/route/{routeID}/dateTime/{dateTime}', 'TicketsController:getTicketsByTrip');
		});
		$this->group('customers/', function () {
	        $this->put('{dni}', 'CustomersController:delete');
	        $this->put('', 'CustomersController:updateCustomerWeb');
		});
		$this->group('trips/', function () {
	        $this->post('', 'TripsController:create');
	        $this->put('route/{routeID}/dateTime/{dateTime}', 'TripsController:delete');
	        $this->put('', 'TripsController:update');
	        $this->post('schedule', 'TripsController:massiveTripsCreation');
	        $this->get('date/{date}', 'TripsController:getAllTripsByDate');
	        $this->get('dateTime/{dateTime}', 'TripsController:getAllTripsFromDateTime');
	        $this->get('route/{routeID}/fromDateTime/{fromDateTime}/toDateTime/{toDateTime}', 'TripsController:getRouteTripsBetweenDates');
		});
	});
});
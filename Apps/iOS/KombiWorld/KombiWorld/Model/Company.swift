//
//  Company.swift
//  KombiWorld
//
//  Created by Alan Nevot on 16/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Company: NSObject {

    var id : Int!
    var name : String!
    var logo : UIImage?

    init(id : Int, name : String, logo : UIImage) {
        self.id = id
        self.name = name
        self.logo = logo
    }
}

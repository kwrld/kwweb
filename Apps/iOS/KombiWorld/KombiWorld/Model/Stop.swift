//
//  Stop.swift
//  KombiWorld
//
//  Created by Alan Nevot on 17/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Stop: NSObject {

    var id : Int!
    var routeId : Int!
    var name : String!
    var address : String!
    var latitude : Double?
    var longitude : Double?
    var adicTime : Int?
    var adicFee : Int?

    init(id : Int, routeId : Int, name : String, address : String) {

        self.id = id
        self.routeId = routeId
        self.name = name
        self.address = address
        
    }
}

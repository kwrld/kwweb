//
//  Schedule.swift
//  KombiWorld
//
//  Created by Alan Nevot on 18/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Schedule: NSObject {

    var time : String!

    init(time : String) {
        self.time = time
    }
}

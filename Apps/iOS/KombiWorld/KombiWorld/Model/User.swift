//
//  User.swift
//  KombiWorld
//
//  Created by Alan Nevot on 28/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class User: NSObject {

    static var currentUser : User?

    var id : Int?
    var username : String?
    var password : String?
    var token : String?
    
    init(username : String) {
        self.username = username
    }
}

//
//  Customer.swift
//  KombiWorld
//
//  Created by Alan Nevot on 29/10/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Customer: NSObject {

    var dni : Int!
    var name : String!
    var lastName : String!
    var sex : String!
    var birthdate : Date!
    var telephone : String?
    var email : String?

    init(dni : Int, name : String, lastName : String, sex : String, birthdate : Date) {

        self.dni = dni
        self.name = name
        self.lastName = lastName
        self.sex = sex
        self.birthdate = birthdate
        
    }
}

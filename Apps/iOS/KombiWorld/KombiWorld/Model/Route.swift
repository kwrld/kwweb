//
//  Route.swift
//  KombiWorld
//
//  Created by Alan Nevot on 16/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Route: NSObject {

    var companyID : Int!
    var routeID : Int!
    var name : String!
    var basePrice : Int?
    var companyName : String?
    var companyLogo : UIImageView?

    init(companyID : Int, routeID : Int, name : String, basePrice : Int) {
        self.companyID = companyID
        self.routeID = routeID
        self.name = name
        self.basePrice = basePrice
    }
    
    init(companyID : Int, routeID : Int, name : String) {
        self.companyID = companyID
        self.routeID = routeID
        self.name = name
    }
}

//
//  Trip.swift
//  KombiWorld
//
//  Created by Alan Nevot on 18/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class Trip: NSObject {

    var company : Company?
    var route : Route?
    var fromStop : Stop?
    var toStop : Stop?
    var schedule : Schedule?
    var ticketDateTime : Date?
    var quantity : Int?
    var totalPrice : Int?

    override init(){
        super.init()
    }

    init(route : Route, fromStop : Stop, toStop : Stop) {
        self.route = route
        self.fromStop = fromStop
        self.toStop = toStop
    }
}

//
//  HistoricUserTickets.swift
//  KombiWorld
//
//  Created by Alan Nevot on 4/7/18.
//  Copyright © 2018 KombiWorld. All rights reserved.
//

import UIKit

class HistoricalUserTickets: NSObject {
    
    var routeName : String!
    var routeId : Int?
    var companyName : String!
    var companyId : Int?
    var tripDatetime : String!
    var ticketDateTime : String!
    var incomeChannel : String!
    var numberOfSeats : String!
    var ticketTotalPrice : String!
    var fromStopId : Int?
    var fromStopName : String!
    var toStopId : Int?
    var toStopName : String!
    var fromStopLat : Double?
    var fromStopLon : Double?
    var toStopLat : Double?
    var toStopLon : Double?
    var fromStopAddress : String?
    var toStopAddress : String?
    
    override init(){
        super.init()
    }
    
    init(routeName : String, companyName : String, tripDateTime : String, ticketDateTime : String, incomeChannel : String, numberOfSeats : String, ticketTotalPrice : String, fromStopName : String, toStopName : String) {
        
        self.routeName = routeName
        self.companyName = companyName
        self.tripDatetime = tripDateTime
        self.ticketDateTime = ticketDateTime
        self.incomeChannel = incomeChannel
        self.numberOfSeats = numberOfSeats
        self.ticketTotalPrice = ticketTotalPrice
        self.fromStopName = fromStopName
        self.toStopName = toStopName
        
    }
}

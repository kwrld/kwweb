//
//  TravelByCompanyViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 6/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT
import SwiftyJSON

class TravelByCompanyViewController: UIViewController {

    @IBOutlet var tableView: UITableView!

    var companies : [Company] = []
    var trip : Trip = Trip()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        self.getCompanies()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "companySelected" {

            let destinationVC = segue.destination as! ChooseRouteViewController

            destinationVC.trip = self.trip

        }
    }

    func getCompanies () {

        Alamofire.request(Router.getAllCompanies()).validate().responseJSON { (response) in

            switch response.result {
                case .success(let value):
                    let json = JSON(value)

                    for item in json["companies"] {

                        let aCompany : Company

                        let id = Int(item.1["companyId"].string!)
                        let name = item.1["companyFantasyName"].string

                        aCompany = Company(id: id!, name: name!, logo: #imageLiteral(resourceName: "logo"))

                        self.companies.append(aCompany)
                    }

                self.tableView.reloadData()

                case .failure(let error):
                    print(error)
            }
        }
    }
}
extension TravelByCompanyViewController : UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.companies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "TravelByCompanyCell") as! TravelByCompanyTableViewCell

        let cellCompany = self.companies[indexPath.row]

        cell.companyNameLabel.text = cellCompany.name
        cell.companyImageView.image = cellCompany.logo

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.trip.company = self.companies[indexPath.row]
        self.trip.quantity = 1
        
        self.performSegue(withIdentifier: "companySelected", sender: nil)

    }
}

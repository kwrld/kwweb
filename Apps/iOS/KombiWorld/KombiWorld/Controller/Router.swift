//
//  Router.swift
//  KombiWorld
//
//  Created by Alan Nevot on 29/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {

    case getAllCompanies()
    case getCompany(companyId: Int)
    case userLogin(parameters: Parameters)
    case userRegister(parameters: Parameters)
    case userDelete(username: String)
    case getUserById(userId: Int)
    case getUserByUsername(username: String)
    case userPasswordReset(username: String, dni: Int)
    case customerCreation(parameters: Parameters)
    case customerProfileEdit(parameters: Parameters)
    case getCustomerByUsername(username : String)
    case getAllRoutesByCompany(companyId: Int)
    case getRoute(companyId: Int, routeId: Int)
    case getStopsAvailabilityByDate(routeId: Int, dateTime: String, fromStop: Int, toStop: Int, quantity: Int)
    case getSpecificTrip(companyId: Int, routeId: Int, dateTime: String)
    case getAllTripsByCompanyAndRoute(companyId: Int, routeId: Int, aDate: String)
    case getNearbyStops(latFrom: Float, lonFrom: Float, latTo: Float, lonTo: Float, distance: Int)
    case getAllRouteStops(routeID: Int)
    case getNextStops(routeID: Int, stopId: Int)
    case createTicket(parameters: Parameters)
    case cancelTicket(companyId: Int, routeId: Int, userId: Int, dateTime: String)
    case editTicket(parameters: Parameters)
    case getAllUserTickets(userId: Int, dateTime: String, quantity: Int)
    case createPreReservation(parameters: Parameters)
    case reservationConfirmation(userId: Int, routeId: Int, tripDateTime: String)
    case reservationCancelation(userId: Int, routeId: Int, tripDateTime: String)
    case getUserHistory(userId: Int)

    static let baseURLString = "http://192.168.0.104:8888/kwweb/Web/api/public/index.php/v1/"

    var method: HTTPMethod {
        switch self {
            /* Companies */
        case .getAllCompanies:
            return .get
        case .getCompany:
            return .get

            /* Users */
        case .userLogin:
            return .post
        case .userRegister:
            return .post
        case .userDelete:
            return .put
        case .getUserById:
            return .get
        case .getUserByUsername:
            return .get
        case .userPasswordReset:
            return .post

            /* People */
        case .customerCreation:
            return .post
        case .customerProfileEdit:
            return .put
        case .getCustomerByUsername:
            return .get

            /* Routes */
        case .getAllRoutesByCompany:
            return .get
        case .getRoute:
            return .get

            /* Trips */
        case .getStopsAvailabilityByDate:
            return .get
        case .getSpecificTrip:
            return .get
        case .getAllTripsByCompanyAndRoute:
            return .get

            /* Stops */
        case .getNearbyStops:
            return .get
        case .getAllRouteStops:
            return .get
        case .getNextStops:
            return .get

            /* Tickets */
        case .createTicket:
            return .post
        case .cancelTicket:
            return .put
        case .editTicket:
            return .put
        case .getAllUserTickets:
            return .get
            
            /* Pre Reservations */
        case .createPreReservation:
            return .post
        case .reservationConfirmation:
            return .post
        case .reservationCancelation:
            return .post
            
            /* History User Tickets */
        case .getUserHistory:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getAllCompanies:
            return "both/companies/"
        case .getCompany(let companyId):
            return "both/companies/\(companyId)"
        case .userLogin:
            return "auth/login"
        case .userRegister:
            return "auth/"
        case .userDelete(let username):
            return "both/users/\(username)"
        case .getUserById(let userId):
            return "both/users/userID/\(userId)"
        case .getUserByUsername(let username):
            return "both/users/username/\(username)"
        case .userPasswordReset(let username, let dni):
            return "both/users/passwordReset/user/\(username)/dni/\(dni)"
        case .customerCreation:
            return "both/customers/"
        case .customerProfileEdit:
            return "both/customers/"
        case .getCustomerByUsername(let username):
            return "both/customers/username/\(username)"
        case .getAllRoutesByCompany(let companyId):
            return "both/routes/company/\(companyId)"
        case .getRoute(let companyId, let routeId):
            return "both/routes/company/\(companyId)/route/\(routeId)"
        case .getStopsAvailabilityByDate(let routeId, let dateTime, let fromStopId, let toStopId, let quantity):
            return "both/trips/availability/routeId/\(routeId)/date/\(dateTime)/seats/\(quantity)/from/\(fromStopId)/to/\(toStopId)"
        case .getSpecificTrip(let companyId, let routeId, let dateTime):
            return "both/trips/company/\(companyId)/route/\(routeId)/dateTime/\(dateTime)"
        case .getAllTripsByCompanyAndRoute(let companyId, let routeId, let aDate):
            return "both/trips/company/\(companyId)/routes/\(routeId)/dateTime/\(aDate)"
        case .getNearbyStops(let latFrom, let lonFrom, let latTo, let lonTo, let distance):
            return "both/stops/latFrom/\(latFrom)/lonFrom/\(lonFrom)/latTo/\(latTo)/lonTo/\(lonTo)/kms/\(distance)"
        case .getAllRouteStops(let routeId):
            return "both/stops/route/\(routeId)"
        case .getNextStops(let routeId, let stopId):
            return "both/stops/route/\(routeId)/stop/\(stopId)"
        case .createTicket:
            return "both/tickets/"
        case .cancelTicket(let companyId, let routeId, let userId, let dateTime):
            return "both/tickets/company/\(companyId)/route/\(routeId)/user/\(userId)/dateTime/\(dateTime)"
        case .editTicket:
            return "both/tickets/"
        case .getAllUserTickets(let userId, let dateTime, let quantity):
            return "both/tickets/user/\(userId)/dateTime/\(dateTime)/qty/\(quantity)"
        case .createPreReservation:
            return "both/prereservations/"
        case .reservationConfirmation(let userId, let routeId, let tripDateTime):
            return "both/prereservations/confirm/user/\(userId)/route/\(routeId)/trip/\(tripDateTime)"
        case .reservationCancelation(let userId, let routeId, let tripDateTime):
            return "both/prereservations/cancel/user/\(userId)/route/\(routeId)/trip/\(tripDateTime)"
        case .getUserHistory(let userId):
            return "both/tickets/history/user/\(userId)"
            
        }
    }
    
    // MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = try Router.baseURLString.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        switch self {
        case .userLogin(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .userRegister(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .customerCreation(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .customerProfileEdit(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .createTicket(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .editTicket(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .createPreReservation(let parameters):
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
        default:
            break
        }
        
        return urlRequest
    }
}

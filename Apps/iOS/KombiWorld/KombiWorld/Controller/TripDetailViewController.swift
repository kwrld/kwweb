//
//  TripDetailViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 15/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import MapKit

class TripDetailViewController: UIViewController {

    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var routeNameLabel: UILabel!
    @IBOutlet weak var tripDateTimeLabel: UILabel!
    @IBOutlet weak var seatsQtyLabel: UILabel!
    @IBOutlet weak var fromStopNameLabel: UILabel!
    @IBOutlet weak var toStopNameLabel: UILabel!
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var tripMapView: MKMapView!
    
    var history : HistoricalUserTickets!
    var trip : Trip?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let auxTicketDate : Date = dateFormatter.date(from: self.history.ticketDateTime!)!
        let auxTripDate : Date = dateFormatter.date(from: self.history.tripDatetime!)!
        
        dateFormatter.dateFormat = "dd MMM 'de' YYYY' a las 'HH:mm'Hs.'"
        self.tripDateTimeLabel.text = "Viajaste el \(dateFormatter.string(from: auxTripDate))"
        
        self.companyNameLabel.text = self.history.companyName
        self.routeNameLabel.text = self.history.routeName
        
        self.putStopsCoordinatesIntoMap()
        self.tripMapView.showAnnotations(self.tripMapView.annotations, animated: true)
        
        if (Int(self.history.numberOfSeats!) == 1)
        {
            self.seatsQtyLabel.text = "\(self.history.numberOfSeats!) Asiento"
        }
        else
        {
            self.seatsQtyLabel.text = "\(self.history.numberOfSeats!) Asientos"
        }
        
        self.fromStopNameLabel.text = "Desde \(self.history.fromStopName!)"
        self.toStopNameLabel.text = "Hasta \(self.history.toStopName!)"
        
        dateFormatter.dateFormat = "dd.MMM.YY HH:mm'Hs.'"
        
        if (self.history.incomeChannel == "MOB") {
            self.originLabel.text = "Creado el \(dateFormatter.string(from: auxTicketDate)) desde KombiWorld App"
        }
        else
        {
            self.originLabel.text = "Creado el \(dateFormatter.string(from: auxTicketDate)) vía telefónica"
        }
        
        self.totalPriceLabel.text = "Total: $\(self.history.ticketTotalPrice!)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func putStopsCoordinatesIntoMap () {
        
        let fromAnnotation = MKPointAnnotation()
        fromAnnotation.coordinate = CLLocationCoordinate2D(latitude: (self.history.fromStopLat)!, longitude: (self.history.fromStopLon)!)
        let toAnnotation = MKPointAnnotation()
        toAnnotation.coordinate = CLLocationCoordinate2D(latitude: (self.history.toStopLat)!, longitude: (self.history.toStopLon)!)
        
        self.tripMapView.addAnnotation(fromAnnotation)
        self.tripMapView.addAnnotation(toAnnotation)
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "setScheduleRepeatTrip" {
            
            let destinationVC = segue.destination as! TimeTableViewController
            
            destinationVC.trip = self.trip
            
        }
    }

    
    @IBAction func repeatTripButtonPressed(_ sender: UIButton) {
        
        let company = Company(id: self.history.companyId!, name: self.history.companyName, logo: UIImage(named: "logo")!)
        let fromStop = Stop(id: self.history.fromStopId!, routeId: self.history.routeId!, name: self.history.fromStopName, address: self.history.fromStopAddress!)
        let toStop = Stop(id: self.history.toStopId!, routeId: self.history.routeId!, name: self.history.toStopName, address: self.history.toStopAddress!)
        let route = Route(companyID: company.id, routeID: self.history.routeId!, name: self.history.routeName!)
        
        fromStop.latitude = self.history.fromStopLat
        fromStop.longitude = self.history.fromStopLon
        toStop.latitude = self.history.toStopLat
        toStop.longitude = self.history.toStopLon
        
        self.trip = Trip(route: route, fromStop: fromStop, toStop: toStop)
        self.trip?.quantity = 1
        self.trip?.company = company
        
        self.performSegue(withIdentifier: "setScheduleRepeatTrip", sender: nil)
    }
}
extension TripDetailViewController : MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let identifier = "Stop"
        
        if annotation .isKind(of: MKUserLocation.self){
            
            return nil
            
        }
        
        var annotationView : MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
            
        }
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))
        
        annotationView?.pinTintColor = UIColor.yellow
        annotationView?.leftCalloutAccessoryView = imageView
        
        return annotationView
    }
}

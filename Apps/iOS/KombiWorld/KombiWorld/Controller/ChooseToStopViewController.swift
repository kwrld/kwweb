//
//  ChooseToStopViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 6/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT
import SwiftyJSON

class ChooseToStopViewController: UIViewController {

    @IBOutlet var toStopTableView: UITableView!

    var stops : [Stop] = []
    var trip : Trip!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getStops()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toStopSelected" {

            let destinationVC = segue.destination as! TimeTableViewController

            destinationVC.trip = self.trip

        }
    }

    func getStops () {

        Alamofire.request(Router.getNextStops(routeID: self.trip.route!.routeID!, stopId: self.trip.fromStop!.id!)).responseJSON { (response) in

            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for item in json["stops"] {
                    
                    let aStop : Stop
                    
                    let stopId = Int(item.1["stopId"].string!)
                    let routeId = Int(item.1["routeId"].string!)
                    let name = item.1["stopName"].string!
                    let address = item.1["stopAddress"].string!
                    let timeAdic = Int(item.1["stopElapsedTime"].string!)
                    let priceAdic = Int(item.1["stopAdditionalFee"].string!)
                    aStop = Stop(id: stopId!, routeId: routeId!, name: name, address: address)
                    aStop.latitude = Double(item.1["latitude"].string!)
                    aStop.longitude = Double(item.1["longitude"].string!)
                    aStop.adicFee = priceAdic
                    aStop.adicTime = timeAdic
                    
                    self.stops.append(aStop)
                }
                
                self.toStopTableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension ChooseToStopViewController : UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stops.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ToStopCell") as! ToStopTableViewCell

        let cellStop = self.stops[indexPath.row]

        cell.stopLabel.text = cellStop.name
        cell.stopAddressLabel.text = cellStop.address

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.trip!.toStop = self.stops[indexPath.row]
        self.performSegue(withIdentifier: "toStopSelected", sender: nil)

    }
}

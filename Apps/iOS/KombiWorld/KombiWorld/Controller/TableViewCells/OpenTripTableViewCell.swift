//
//  OpenTripTableViewCell.swift
//  KombiWorld
//
//  Created by Alan Nevot on 16/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class OpenTripTableViewCell: UITableViewCell {

    @IBOutlet var routeLabel: UILabel!
    @IBOutlet var dateTimeLabel: UILabel!
    @IBOutlet var companyLabel: UILabel!
    @IBOutlet var companyImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

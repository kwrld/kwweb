//
//  MyTripTableViewCell.swift
//  KombiWorld
//
//  Created by Alan Nevot on 16/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class MyTripTableViewCell: UITableViewCell {

    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var routeNameLabel: UILabel!
    @IBOutlet weak var tripDateTimeLabel: UILabel!
    @IBOutlet weak var companyLogoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

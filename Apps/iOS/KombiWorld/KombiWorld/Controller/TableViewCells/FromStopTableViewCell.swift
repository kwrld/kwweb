//
//  FromStopTableViewCell.swift
//  KombiWorld
//
//  Created by Alan Nevot on 16/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit

class FromStopTableViewCell: UITableViewCell {

    @IBOutlet var stopLabel: UILabel!
    @IBOutlet var stopAddressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

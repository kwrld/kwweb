//
//  RoutesFromLocationTableViewCell.swift
//  KombiWorld
//
//  Created by Alan Nevot on 21/10/18.
//  Copyright © 2018 KombiWorld. All rights reserved.
//

import UIKit

class RoutesFromLocationTableViewCell: UITableViewCell {

    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var routeNameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CompaniesTableViewCell.swift
//  KombiWorld
//
//  Created by Alan Nevot on 15/7/18.
//  Copyright © 2018 KombiWorld. All rights reserved.
//

import UIKit

class CompaniesTableViewCell: UITableViewCell {

    @IBOutlet weak var companyLogoImageView: UIImageView!
    @IBOutlet weak var companyDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

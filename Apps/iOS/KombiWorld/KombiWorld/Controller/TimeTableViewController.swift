//
//  TimeTableViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 6/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import JWT

class TimeTableViewController: UIViewController {

    @IBOutlet var timeTableView: UITableView!
    @IBOutlet var datePicker: UIDatePicker!

    var trip : Trip!
    var schedules : [Schedule] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        if segue.identifier == "scheduleSelected" {

            let destinationVC = segue.destination as! TicketDetailViewController

            destinationVC.trip = self.trip

        }
    }

    @IBAction func findButtonPressed(_ sender: UIButton) {

        let sessionManager = SessionManager()
        sessionManager.adapter = AccessTokenAdapter(accessToken: (User.currentUser?.token)!)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        let stringDate = dateFormatter.string(from: self.datePicker.date)
        self.trip.quantity = 1
        
        Alamofire.request(Router.getStopsAvailabilityByDate(routeId: self.trip.route!.routeID!, dateTime: stringDate, fromStop: self.trip.fromStop!.id, toStop: self.trip.toStop!.id, quantity: self.trip.quantity!)).responseJSON { (response) in
            
            self.schedules = []
            self.timeTableView.reloadData()
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for item in json["availability"] {
                    
                    let aSchedule : Schedule
                    let time = item.1["tripDateTime"].string!
                    
                    aSchedule = Schedule(time: time)

                    self.schedules.append(aSchedule)

                }
                
                self.timeTableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }

    }
}

extension TimeTableViewController : UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schedules.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell") as! ScheduleTableViewCell

        let cellSchedule = self.schedules[indexPath.row]

        let fullDateTimeArray = cellSchedule.time.components(separatedBy: " ")

        cell.timeLabel.text = "\(fullDateTimeArray[1])Hs."

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.trip.schedule = self.schedules[indexPath.row] as Schedule

        let aReservation = [
            "preReservation" : [
                "userId": User.currentUser!.id!,
                "routeId": self.trip.route!.routeID,
                "tripDateTime": self.trip.schedule!.time,
                "firstStopId": self.trip.fromStop!.id,
                "lastStopId": self.trip.toStop!.id,
                "reqSeats": /*self.trip.quantity!*/ 1,
                "incomeChannel": "MOB"
            ]
        ]

        Alamofire.request(Router.createPreReservation(parameters: aReservation)).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                let reservations = json["reservations"][0]

                self.trip.totalPrice = Int(reservations["totalPrice"].string!)
                
                self.performSegue(withIdentifier: "scheduleSelected", sender: nil)
                
            case .failure(let error):
                print(error)
            }
        }
    }
}

//
//  TicketDetailViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 6/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import MapKit
import SwiftyJSON

class TicketDetailViewController: UIViewController {

    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet var routeLabel: UILabel!
    @IBOutlet var fromStopLabel: UILabel!
    @IBOutlet var toStopLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var tripMapView: MKMapView!

    var trip : Trip!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM 'de' YYYY' a las 'HH:mm'Hs.'"
        //"Viajas el \(dateFormatter.string(from: self.trip.ticketDateTime!))"
        
        self.companyLabel.text = self.trip.company!.name!
        self.routeLabel.text = self.trip.route!.name
        self.fromStopLabel.text = "Desde \(self.trip.fromStop!.name!)"
        self.toStopLabel.text = "Hasta \(self.trip.toStop!.name!)"
        self.totalPriceLabel.text = "$ \(self.trip.totalPrice!)"

        self.putStopsCoordinatesIntoMap()
        
        self.tripMapView.showAnnotations(self.tripMapView.annotations, animated: true)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func centerMapOnLocation(location: CLLocation) {

        let regionRadius : CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)

        self.tripMapView.setRegion(coordinateRegion, animated: true)
    }

    func putStopsCoordinatesIntoMap () {

        let fromAnnotation = MKPointAnnotation()
        fromAnnotation.coordinate = CLLocationCoordinate2D(latitude: (self.trip.fromStop?.latitude)!, longitude: (self.trip.fromStop?.longitude)!)
        let toAnnotation = MKPointAnnotation()
        toAnnotation.coordinate = CLLocationCoordinate2D(latitude: (self.trip.toStop?.latitude)!, longitude: (self.trip.toStop?.longitude)!)

        self.tripMapView.addAnnotation(fromAnnotation)
        self.tripMapView.addAnnotation(toAnnotation)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func confirmButtonPressed(_ sender: UIButton) {

        Alamofire.request(Router.reservationConfirmation(userId: User.currentUser!.id!, routeId: self.trip.route!.routeID!, tripDateTime: self.trip.schedule!.time)).responseJSON { response in
            
            switch response.result {
            case .success(let value):
                
                print("Pasaje creado exitosamente! \(value)")
                self.trip = nil
                self.performSegue(withIdentifier: "backToMainVC", sender: self)
                
            case .failure( _):
                if let data = response.data {
                    let json = String(data: data, encoding: String.Encoding.utf8)
                    print("Failure Response: \(String(describing: json))")
                }
            }
        }
    }
}

extension TicketDetailViewController : MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {

        let identifier = "Stop"

        if annotation .isKind(of: MKUserLocation.self){

            return nil

        }

        var annotationView : MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView

        if annotationView == nil {

            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true

        }

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 52, height: 52))

        annotationView?.pinTintColor = UIColor.yellow
        annotationView?.leftCalloutAccessoryView = imageView

        return annotationView
    }
}

//
//  LoginViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 25/10/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT

class LoginViewController: UIViewController {

    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    var token : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func validateRequiredFields () -> Bool {
        
        if self.usernameTextField.text == "" ||
            self.usernameTextField.text == nil {
            
            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu nombre de usuario")
            return false
        }
        
        if self.passwordTextField.text == "" ||
            self.passwordTextField.text == nil {
            
            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu contraseña")
            return false
        }
        
        return true
        
    }
    
    func showUserAlert(title : String, message : String) {
        
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction : UIAlertAction = UIAlertAction(title: title, style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        self.show(alertController, sender: self)
        
    }
    
    func clearFields () {
        
        self.usernameTextField.text = ""
        self.passwordTextField.text = ""
        
    }
    
    @IBAction func signUpPressed(_ sender: UIButton) {

        self.performSegue(withIdentifier: "signUpVC", sender: self)

    }

    @IBAction func signInPressed(_ sender: UIButton) {

        if !self.validateRequiredFields() {
            
            self.clearFields()
            return
        }
        
        let username : String = self.usernameTextField.text!
        let password : String = self.passwordTextField.text!

        let userData = [
            "user" : [
                "username" : username,
                "password" : password
            ]
        ]
        /*
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            "https://localhost:4430": .disableEvaluation,
            "https://localhost": .disableEvaluation,
            "https://192.168.0.2:4430": .disableEvaluation,
            "https://192.168.0.2": .disableEvaluation
        ]
        
        let sessionManager = SessionManager(
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )*/

        let sessionManager = SessionManager()
        sessionManager.adapter = AccessTokenAdapter(accessToken: "")
        
        Alamofire.request(Router.userLogin(parameters: userData)).validate().responseJSON { response in

            print(response)

            if let authToken : String = response.response?.allHeaderFields["Authorization"] as? String {

                do {
                    
                    self.token = authToken.components(separatedBy: "Bearer ")[1]
                    
                    let claims: ClaimSet = try JWT.decode(self.token!, algorithm: .hs256("corsita2008".data(using: .utf8)!))
                    
                    User.currentUser = User(username: claims["username"] as! String)
                    User.currentUser!.id = Int(claims["id"] as! String)
                    User.currentUser!.token = self.token
                    
                    print("Bienvenido \(User.currentUser!.username!)")
                    
                    self.performSegue(withIdentifier: "goToMainVC", sender: self)
                    
                } catch {
                    
                    self.showUserAlert(title: "Oops!", message: "Ocurrió un error, intentalo nuevamente")

                    print("Failed to decode JWT: \(error)")
                    
                }
            }
            else
            {
                self.showUserAlert(title: "Oops!", message: "Usuario o contraseña incorrectos. Intentalo nuevamente")

                print("Usuario no autorizado.")
            }
        }
    }
}
extension LoginViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

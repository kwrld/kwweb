//
//  TravelViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 15/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MyTripsViewController: UIViewController {
    
    @IBOutlet var tripsTableView: UITableView!
    
    var history : [HistoricalUserTickets] = []
    var selectedHistory : HistoricalUserTickets!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.getUserHistory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showHistoryDetail" {
            
            let destinationVC = segue.destination as! TripDetailViewController

            destinationVC.history = self.selectedHistory
            
        }
    }
    
    func getUserHistory () {
        
        Alamofire.request(Router.getUserHistory(userId: User.currentUser!.id!)).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)

                for item in json["history"] {
                    
                    let routeName : String = item.1["routeName"].string!
                    let companyName : String = item.1["companyName"].string!
                    let companyId : Int = Int(item.1["companyId"].string!)!
                    let routeId : Int = Int(item.1["routeId"].string!)!
                    let tripDatetime : String = item.1["tripDateTime"].string!
                    let ticketDateTime : String = item.1["ticketDateTime"].string!
                    let incomeChannel : String = item.1["incomeChannel"].string!
                    let numberOfSeats : String = item.1["numberOfSeats"].string!
                    let ticketTotalPrice : String = item.1["ticketTotalPrice"].string!
                    let fromStopId : Int = Int(item.1["fromStopId"].string!)!
                    let fromStopName : String = item.1["fromStopName"].string!
                    let toStopId : Int = Int(item.1["toStopId"].string!)!
                    let toStopName : String = item.1["toStopName"].string!
                    let fromStopLat : Double = Double(item.1["fromStopLat"].string!)!
                    let fromStopLon : Double = Double(item.1["fromStopLon"].string!)!
                    let fromStopAddress : String = item.1["fromStopAddress"].string!
                    let toStopLat : Double = Double(item.1["toStopLat"].string!)!
                    let toStopLon : Double = Double(item.1["toStopLon"].string!)!
                    let toStopAddress : String = item.1["toStopAddress"].string!
                    

                    let aTicket : HistoricalUserTickets = HistoricalUserTickets(routeName: routeName, companyName: companyName, tripDateTime: tripDatetime, ticketDateTime: ticketDateTime, incomeChannel: incomeChannel, numberOfSeats: numberOfSeats, ticketTotalPrice: ticketTotalPrice, fromStopName: fromStopName, toStopName: toStopName)
                    
                    aTicket.companyId = companyId
                    aTicket.fromStopId = fromStopId
                    aTicket.fromStopLat = fromStopLat
                    aTicket.fromStopLon = fromStopLon
                    aTicket.fromStopAddress = fromStopAddress
                    aTicket.toStopId = toStopId
                    aTicket.toStopLat = toStopLat
                    aTicket.toStopLon = toStopLon
                    aTicket.toStopAddress = toStopAddress
                    aTicket.routeId = routeId
                    
                    self.history.append(aTicket)
                }
                
                self.tripsTableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension MyTripsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.history.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as! MyTripTableViewCell
        
        let historyCell = self.history[indexPath.row]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let auxTripDate : Date = dateFormatter.date(from: historyCell.tripDatetime!)!
        
        dateFormatter.dateFormat = "dd MMM 'de' YYYY' a las 'HH:mm'Hs.'"
        cell.tripDateTimeLabel.text = "Viajaste el \(dateFormatter.string(from: auxTripDate))"
        
        cell.companyLogoImageView.image = UIImage(named: "logo")
        cell.companyNameLabel.text = historyCell.companyName
        cell.routeNameLabel.text = historyCell.routeName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedHistory = self.history[indexPath.row]
        self.performSegue(withIdentifier: "showHistoryDetail", sender: nil)
        
    }
}

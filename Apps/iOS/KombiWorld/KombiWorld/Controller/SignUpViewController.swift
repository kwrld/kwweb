//
//  SignUpViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 26/10/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import AVFoundation
import QRCodeReader
import Alamofire

class SignUpViewController: UIViewController {

    var newUser : User?
    var customer : Customer?
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordVerifyTextField: UITextField!
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.pdf417], captureDevicePosition: .back)
        }

        return QRCodeReaderViewController(builder: builder)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "showScanDetail" {

            let destinationVC = segue.destination as! ScanDetailViewController
            
            destinationVC.newUser = self.newUser
            destinationVC.customer = self.customer

        } else if segue.identifier == "manualInput" {

            let destinationVC = segue.destination as! ManualInputViewController

            destinationVC.newUser = self.newUser
            
        }
    }
    
    func showUserAlert(title : String, message : String) {
        
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction : UIAlertAction = UIAlertAction(title: title, style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        self.show(alertController, sender: self)
        
    }
    
    func validateRequiredFields () -> Bool {
        
        if self.usernameTextField.text == "" ||
            self.usernameTextField.text == nil {
            
            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu nombre de usuario")
            return false
        }
        
        if self.passwordTextField.text == "" ||
            self.passwordTextField.text == nil {
            
            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu contraseña")
            return false
        }
        
        if self.passwordVerifyTextField.text == "" ||
            self.passwordVerifyTextField.text == nil {
            
            self.showUserAlert(title: "Oops!", message: "Debes repetir tu contraseña")
            return false
        }
        
        if self.passwordTextField.text != self.passwordVerifyTextField.text {
            
            self.showUserAlert(title: "Oops!", message: "Las contraseñas ingresadas deben coincidir")
            return false
        }
        
        return true
        
    }
    
    func clearFields () {
        
        self.usernameTextField.text = ""
        self.passwordTextField.text = ""
        self.passwordVerifyTextField.text = ""
        
    }

    @IBAction func manualInputButtonPressed(_ sender: UIButton) {

        if !self.validateRequiredFields() {

            self.clearFields()
            return
        }

        self.newUser = User(username: self.usernameTextField.text!)
        self.newUser!.password = self.passwordTextField.text!

        self.performSegue(withIdentifier: "manualInput", sender: nil)
    }
}

extension SignUpViewController : QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()

        dismiss(animated: true, completion: nil)
    }

    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()

        dismiss(animated: true, completion: nil)
    }

    @IBAction func scanAction(_ sender: AnyObject) {

        if !self.validateRequiredFields() {
            
            self.clearFields()
            return
        }
        
        // Retrieve the QRCode content
        // By using the delegate pattern
        self.readerVC.delegate = self as QRCodeReaderViewControllerDelegate

        // Or by using the closure pattern
        self.readerVC.completionBlock = { (result: QRCodeReaderResult?) in

            if let result = result {

                let resultsArray = result.value.components(separatedBy: "@")

                let lastName = resultsArray[1].capitalized
                let name = resultsArray[2].capitalized
                let sex = resultsArray[3]
                let dni = resultsArray[4]
                let stringBirthdate = resultsArray[6]

                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .short
                dateFormatter.timeStyle = .none

                let birthdate : Date = dateFormatter.date(from: stringBirthdate)!

                self.newUser = User(username: self.usernameTextField.text!)
                self.newUser?.password = self.passwordTextField.text!

                self.customer = Customer(dni: Int(dni)!, name: name, lastName: lastName, sex: sex, birthdate: birthdate)

                self.performSegue(withIdentifier: "showScanDetail", sender: nil)

            }
        }

        // Presents the readerVC as modal form sheet
        self.readerVC.modalPresentationStyle = .formSheet
        
        present(self.readerVC, animated: true, completion: nil)
    }
}
extension SignUpViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}

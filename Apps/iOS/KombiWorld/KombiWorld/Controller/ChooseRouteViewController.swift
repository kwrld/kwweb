//
//  ChooseRouteViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 6/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT
import SwiftyJSON

class ChooseRouteViewController: UIViewController {

    @IBOutlet var routesTableView: UITableView!

    var trip : Trip!
    var routes : [Route] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getRoutes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        if segue.identifier == "routeSelected" {

            let destinationVC = segue.destination as! ChooseFromStopViewController

            destinationVC.trip = self.trip

        }
    }
    
    func getRoutes () {

        Alamofire.request(Router.getAllRoutesByCompany(companyId: self.trip.company!.id!)).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for item in json["routes"] {
                    
                    let aRoute : Route
                    
                    let companyId = Int(item.1["companyId"].string!)
                    let routeId = Int(item.1["routeId"].string!)
                    let name = item.1["name"].string
                    let price = Int(item.1["baseRate"].string!)
                    
                    aRoute = Route(companyID: companyId!, routeID: routeId!, name: name!, basePrice: price!)
                    
                    self.routes.append(aRoute)

                }
                
                self.routesTableView.reloadData()
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension ChooseRouteViewController : UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.routes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell") as! TripTableViewCell

        let cellRoute = self.routes[indexPath.row]

        cell.routeNameLabel.text = cellRoute.name
        //cell.fromStopLabel.text = cellRoute?.fromStopName
        //cell.toStopLabel.text = cellRoute?.toStopName
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.trip.route = self.routes[indexPath.row]
        self.performSegue(withIdentifier: "routeSelected", sender: nil)

    }
}

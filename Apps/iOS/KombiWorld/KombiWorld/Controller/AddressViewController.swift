//
//  AddressViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 7/8/18.
//  Copyright © 2018 KombiWorld. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON

class AddressViewController: UIViewController {
    
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var routesTableView: UITableView!
    var fromSelectedLocation = CLLocationCoordinate2D()
    var toSelectedLocation = CLLocationCoordinate2D()
    var tripResults : [Trip] = []
    var selectedTrip : Trip?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.fromTextField.addTarget(self, action: #selector(self.searchFromLocation), for: .touchDown)
        self.toTextField.addTarget(self, action: #selector(self.searchToLocation), for: .touchDown)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "chooseSchedule" {
            
            let destinationVC = segue.destination as! TimeTableViewController
            
            destinationVC.trip = self.selectedTrip
            
        }
    }
    
    @IBAction func routesSearchButtonPressed(_ sender: UIButton) {

    
        Alamofire.request(Router.getNearbyStops(latFrom: Float(self.fromSelectedLocation.latitude), lonFrom: Float(self.fromSelectedLocation.longitude), latTo: Float(self.toSelectedLocation.latitude), lonTo: Float(self.toSelectedLocation.longitude), distance: 1)).validate().responseJSON { (response) in
            
            switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    for item in json["stops"] {
    
                        let aCompany : Company = Company(id: Int(item.1["companyId"].string!)!, name: item.1["companyName"].string!, logo: UIImage(named: "logo")!)
                        
                        let aRoute : Route = Route(companyID: aCompany.id!, routeID: Int(item.1["routeId"].string!)!, name: item.1["routeName"].string!, basePrice: Int(item.1["routeBaseRate"].string!)!)
                        
                        let fromStop : Stop = Stop(id: Int(item.1["fromStopId"].string!)!, routeId: aRoute.routeID!, name: item.1["fromStopName"].string!, address: item.1["fromStopAddress"].string!)
                        
                        fromStop.latitude = Double(item.1["fromStopLat"].string!)!
                        fromStop.longitude = Double(item.1["fromStopLon"].string!)!

                        
                        let toStop : Stop = Stop(id: Int(item.1["toStopId"].string!)!, routeId: aRoute.routeID!, name: item.1["toStopName"].string!, address: item.1["toStopAddress"].string!)
                        
                        toStop.latitude = Double(item.1["toStopLat"].string!)!
                        toStop.longitude = Double(item.1["toStopLon"].string!)!
                        
                        let aTrip : Trip = Trip(route: aRoute, fromStop: fromStop, toStop: toStop)

                        self.tripResults.append(aTrip)
                    }
                    
                    self.routesTableView.reloadData()
                
                case .failure(let error):
                    print(error)
            }
        }
    }
    
    @objc func searchFromLocation () {
        
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "TravelByLocation") as! TravelByLocationViewController
        
        locationSearchTable.fromLocationSelected = true
        
        self.navigationController?.isNavigationBarHidden = false
        
        let nc = NotificationCenter.default
        
        nc.addObserver(self, selector: #selector(self.getFromLocationData), name: Notification.Name(rawValue: "address"), object: nil)
        
        self.navigationController?.show(locationSearchTable, sender: self)
    }
    
    @objc func searchToLocation () {
        
        let locationSearchTable = storyboard!.instantiateViewController(withIdentifier: "TravelByLocation") as! TravelByLocationViewController
        locationSearchTable.fromLocationSelected = false
        
        self.navigationController?.isNavigationBarHidden = false
        
        let nc = NotificationCenter.default
        
        nc.addObserver(self, selector: #selector(self.getToLocationData), name: Notification.Name(rawValue: "address"), object: nil)
        
        self.navigationController?.show(locationSearchTable, sender: self)
    }
    
    @objc func getFromLocationData (notification:Notification) {
        
        let userInfo:Dictionary<String,MKPlacemark> = notification.userInfo as! Dictionary<String,MKPlacemark>
        
        let item : MKPlacemark = userInfo["selectedLocation"]!
        
        let nc = NotificationCenter.default
        
        nc.removeObserver(self, name: Notification.Name(rawValue: "address"), object: nil)
        
        self.fromTextField.text = item.title!
    }

    @objc func getToLocationData (notification:Notification) {
        
        let userInfo:Dictionary<String,MKPlacemark> = notification.userInfo as! Dictionary<String,MKPlacemark>
        
        let item : MKPlacemark = userInfo["selectedLocation"]!
        
        let nc = NotificationCenter.default
        
        nc.removeObserver(self, name: Notification.Name(rawValue: "address"), object: nil)
        
        self.toTextField.text = item.title!
    }
    
    @IBAction func unwindFromTravelByLocation (_ sender : UIStoryboardSegue) {
        
        if sender.source is TravelByLocationViewController {
            
            if let senderVC = sender.source as? TravelByLocationViewController {
                
                if senderVC.fromLocationSelected {
                    self.fromSelectedLocation = senderVC.selectedLocation
                    
                }
                else
                {
                    self.toSelectedLocation = senderVC.selectedLocation
                }
                
                print("From: \(self.fromSelectedLocation)")
                print("To: \(self.toSelectedLocation)")
            }
        }
    }
}
extension AddressViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tripResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "routeByLocationCell") as! RoutesFromLocationTableViewCell
        
        let tripCell = self.tripResults[indexPath.row]
        
        cell.companyNameLabel.text = tripCell.company?.name!
        cell.routeNameLabel.text = tripCell.route?.name!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedTrip = self.tripResults[indexPath.row]
        self.performSegue(withIdentifier: "chooseSchedule", sender: nil)
        
    }
}

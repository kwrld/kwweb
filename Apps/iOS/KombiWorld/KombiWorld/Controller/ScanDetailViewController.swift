//
//  ScanDetailViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 26/10/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT

class ScanDetailViewController: UIViewController {
    
    var newUser : User!
    var customer : Customer!
    var token : String?
    
    @IBOutlet var dniLabel: UILabel!
    @IBOutlet var welcomeLabel: UILabel!
    @IBOutlet var birthdateLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        if let aCustomer = self.customer {
            
            if aCustomer.sex == "F" {
                self.welcomeLabel.text = "Bienvenida \(aCustomer.name!) \(aCustomer.lastName!)"
            }
            else
            {
                self.welcomeLabel.text = "Bienvenido \(aCustomer.name!) \(aCustomer.lastName!)"
            }
            
            self.dniLabel.text = String(aCustomer.dni)
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .none
            
            self.birthdateLabel.text = dateFormatter.string(from: aCustomer.birthdate)
            self.emailLabel.text = self.newUser!.username!
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
    }
    
    func showUserAlert(title : String, message : String) {
        
        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction : UIAlertAction = UIAlertAction(title: title, style: .default, handler: nil)
        
        alertController.addAction(alertAction)
        
        self.show(alertController, sender: self)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func startButtonPressed(_ sender: UIButton) {

        let userData = [
            "user" : [
                "username" : self.newUser!.username!,
                "password" : self.newUser!.password!,
                "dni" : self.customer!.dni,
                "customer" : [
                    "dni" : self.customer!.dni,
                    "name" : self.customer!.name,
                    "lastName" : self.customer!.lastName,
                    "sex" : self.customer!.sex,
                    "birthdate" : self.customer!.birthdate,
                    "mail" : self.newUser!.username!,
                    "telephone" : "1144469173"
                ]
            ]
        ]
        
        let sessionManager = SessionManager()
        sessionManager.adapter = AccessTokenAdapter(accessToken: "")
        
        Alamofire.request(Router.userRegister(parameters: userData)).responseString { response in
            
            if let authToken : String = response.response?.allHeaderFields["Authorization"] as? String {
                
                do {
                    
                    self.token = authToken.components(separatedBy: "Bearer ")[1]
                    
                    let claims: ClaimSet = try JWT.decode(self.token!, algorithm: .hs256("corsita2008".data(using: .utf8)!))
                    
                    User.currentUser = User(username: claims["username"] as! String)
                    
                    self.showUserAlert(title: "Bienvenido!", message: "Usuario creado exitosamente")
                    
                    print("Bienvenido \(User.currentUser!.username!)")
                    
                    self.performSegue(withIdentifier: "goToMainVC", sender: self)
                    
                } catch {
                    
                    self.showUserAlert(title: "Oops!", message: "Ocurrió un error, intentalo nuevamente")
                    
                    print("Failed to decode JWT: \(error)")
                    
                }
            }
            else
            {
                self.showUserAlert(title: "Oops!", message: "Usuario o contraseña incorrectos. Intentalo nuevamente")
                
                print("Usuario no autorizado.")
            }
            print(response)
        }
    }
}

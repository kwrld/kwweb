//
//  ManualInputViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 30/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import JWT

class ManualInputViewController: UIViewController {
    
    @IBOutlet weak var documentTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var sexSegment: UISegmentedControl!
    @IBOutlet weak var birtdayDatePicker: UIDatePicker!

    var newUser : User!
    var newCustomer : Customer?
    var token : String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "goToMainVC" {

            // Do nothing

        }
    }

    func validateRequiredFields () -> Bool {

        if self.documentTextField.text == "" ||
            self.documentTextField.text == nil {

            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu documento")
            return false
        }

        if self.nameTextField.text == "" ||
            self.nameTextField.text == nil {

            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu nombre")
            return false
        }

        if self.lastNameTextField.text == "" ||
            self.lastNameTextField.text == nil {

            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu apellido")
            return false
        }

        if self.telephoneTextField.text == "" ||
            self.telephoneTextField.text == nil {

            self.showUserAlert(title: "Oops!", message: "Debes ingresar tu teléfono")
            return false
        }

        return true

    }

    func showUserAlert(title : String, message : String) {

        let alertController : UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let alertAction : UIAlertAction = UIAlertAction(title: title, style: .default, handler: nil)

        alertController.addAction(alertAction)

        self.show(alertController, sender: self)

    }

    func clearFields () {

        self.documentTextField.text = ""
        self.nameTextField.text = ""
        self.lastNameTextField.text = ""
        self.telephoneTextField.text = ""

    }

    @IBAction func saveButtonPressed(_ sender: UIButton) {

        let sex : String

        if self.sexSegment.selectedSegmentIndex == 0
        {
            sex = "M"
        }
        else if self.sexSegment.selectedSegmentIndex == 1
        {
            sex = "F"
        }
        else
        {
            sex = "E"
            print("Esto sí que no lo esperábamos... E de error entonces.")
        }

        self.newCustomer = Customer(dni: Int(self.documentTextField.text!)!, name: self.nameTextField.text!, lastName: self.lastNameTextField.text!, sex: sex, birthdate: self.birtdayDatePicker.date)

        _ = [
            "user" : [
                "username" : self.newUser!.username!,
                "password" : self.newUser!.password!,
                "dni" : self.newCustomer!.dni
            ]
        ]

        let customerData = [
            "customer" : [
                "dni" : self.newCustomer!.dni,
                "name" : self.newCustomer!.name,
                "lastName" : self.newCustomer!.lastName,
                "sex" : self.newCustomer!.sex,
                "birthdate" : self.newCustomer!.birthdate,
                "mail" : self.newUser!.username!,
                "telephone" : self.telephoneTextField.text!
            ]
        ]

        let sessionManager = SessionManager()
        sessionManager.adapter = AccessTokenAdapter(accessToken: "")

        Alamofire.request(Router.customerCreation(parameters: customerData)).responseJSON { response in

            switch response.result {
            case .success(_):

                print("Registrado con éxito.")
                self.performSegue(withIdentifier: "goToMainVC", sender: self)

            case .failure(_):
                print("Failure Response: \(response)")
            }
        }
    }
}

extension ManualInputViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//
//  UserProfileViewController.swift
//  KombiWorld
//
//  Created by Alan Nevot on 15/11/17.
//  Copyright © 2017 KombiWorld. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UserProfileViewController: UIViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var sexSwitch: UISegmentedControl!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var passwordRepeatTextField: UITextField!
    @IBOutlet var birthDatePicker: UIDatePicker!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getCustomerInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func saveButtonPressed(_ sender: UIButton) {

        let name : String = self.nameTextField.text!
        let lastName : String = self.lastNameTextField.text!
        var gender : String = "M"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let birthdateAsDate : String = dateFormatter.string(from: self.birthDatePicker.date)
        
        if (self.sexSwitch.selectedSegmentIndex == 0) {
            gender = "M"
        }
        else
        {
            if (self.sexSwitch.selectedSegmentIndex == 1){
                gender = "F"
            }
            else
            {
                gender = "M"
            }
        }
        
        let userData = [
            "customer" : [
                "username" : User.currentUser!.username!,
                "name" : name,
                "lastName" : lastName,
                "birthdate": birthdateAsDate,
                "email": "",
                "telephone": "",
                "gender": gender
            ]
        ]

        Alamofire.request(Router.customerProfileEdit(parameters: userData)).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                print(value)
                
            case .failure(let error):
                print(error)
            }
        }
    }
 
    func getCustomerInfo() {
     
        Alamofire.request(Router.getCustomerByUsername(username: (User.currentUser?.username)!)).responseJSON { (response) in
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                for item in json["customer"] {
                    
                    let customerId = Int(item.1["customerId"].string!)
                    let name = item.1["firstName"].string!
                    let lastname = item.1["lastName"].string!
                    let birthdate = item.1["birthdate"].string!

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let birthdateAsDate : Date = dateFormatter.date(from: birthdate)!

                    let telephone = item.1["telephone"].string!
                    let email = item.1["email"].string!
                    let gender = item.1["gender"].string!

                    let aCustomer : Customer = Customer(dni: customerId!, name: name, lastName: lastname, sex: gender, birthdate: birthdateAsDate)

                    aCustomer.telephone = telephone
                    aCustomer.email = email
                    
                    self.nameTextField.text = aCustomer.name
                    self.lastNameTextField.text = aCustomer.lastName
                    self.birthDatePicker.date = aCustomer.birthdate

                }
                
            case .failure(let error):
                print(error)
            }
        }
    }
}
